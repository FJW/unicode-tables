#! /usr/bin/python

from typing import DefaultDict


def codepoint_to_str(codepoint: int) -> str:
    match codepoint:
        case 0:
            return '[0]'
        case 0x200d:
            return '[ZWJ]'
        case 0x1F3FB:
            return '[light skin]'
        case 0x1F3FC:
            return '[medium-light skin]'
        case 0x1F3FD:
            return '[medium skin]'
        case 0x1F3FE:
            return '[medium-dark skin]'
        case 0x1F3FF:
            return '[dark skin]'
        case 0xfe0f:
            return '[VS:E]'
        case x:
            return chr(x)


def parse_file(filename: str) -> dict[tuple[int, int], list[str]]:
    ret = DefaultDict[tuple[int, int], list](lambda: [])
    for line in open(filename):
        line = line.removeprefix('/* ')
        node_name, tail = line.split('*/',1)
        node = int(node_name, base=16)
        tail  = tail.split(',')
        num_values = int(tail[0])
        edges = [int(value, base=16) for value  in tail[1:1+num_values]]
        for edge in edges:
            codepoint = edge >> 12
            destination = edge & 0xfff
            ret[node, destination].append(codepoint_to_str(codepoint))
    return ret


def to_dot(data: dict[tuple[int, int], list[str]]) -> str:
    ret = ''
    for (source, destination), label in data.items():
        ret += f'"{source}" -> "{destination}" [label="{",".join(label)}"]\n'
    return ret


def main(args: list[str]) -> int:
    if len(args) < 2:
        return 1
    print('digraph { \n', to_dot(parse_file(args[1])), '\n}')
    return 0


if __name__ == "__main__":
    import sys
    sys.exit(main(sys.argv))
