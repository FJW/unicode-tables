#include "uct/split.hpp"
#include <clp/abbr.hpp>
#include <fstream>
#include <iostream>

int main(int argc, const char** argv) {
	using namespace clp::abbr;
	auto parser = arg_parser{
	        def<"w,width", unsigned>{80u},
	        def<"i,input", std::string>{"/dev/stdin"},
	        def<"o,output", std::string>{"/dev/stdout"},
	        flg<"h,help">{},
	};
	const auto [width, input, output, help] = parser.parse(argc, argv);
	if (help) {
		parser.print_help(argv[0], std::cout);
		return 0;
	}

	auto in = std::ifstream{input};
	auto out = std::ofstream{output};
	for (auto line = std::string{}; std::getline(in, line);) {
		const auto lines = uct::split_words(line, width);
		for (const auto [l, hyphen, lw] : lines) {
			out << l << (hyphen ? "-\n" : "\n");
		}
	}
}
