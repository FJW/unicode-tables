#ifndef CLP_OPTION_BASE_HPP
#define CLP_OPTION_BASE_HPP

#include <string>

#include <limits>

#include "fixed_string.hpp"
#include "type_support.hpp"
#include "dependency_checker.hpp"

#include <uct/width.hpp>

namespace clp {
constexpr auto unlimited_invocations = std::numeric_limits<unsigned>::max();

struct invocation_range {
	unsigned min = 0u;
	unsigned max = 1u;
};

struct argument_count {
	unsigned n = 1u;
};

struct description {
	std::string desc;
};

template <typename Storage_Type>
struct parsing_state {
	Storage_Type value;
	unsigned invocations = 0;
};

struct option_info {
	std::string short_name = "";
	std::string long_name = "";
	std::string argument_type = "";
	std::string default_argument = "";
	bool required = false;
	bool always_available = false;
	std::string description = "";
};

template <impl::fixed_string Name, typename Self, typename Result, typename Storage_Type>
class option_base {
public:
	template <typename... Settings>
	option_base(Storage_Type default_value, Settings... settings)
	        : m_initial_state{std::move(default_value), 0} {
		(void)std::initializer_list<int>{(setup(settings), 0)...};
	}

	static constexpr auto name() { return impl::extract_identifier_name<Name>(); }

	using result_type = Result;
	using storage_type = Storage_Type;
	using state = parsing_state<Storage_Type>;

	state initial_state() const { return m_initial_state; }

	result_type get(const state& state) const {
		verify_sufficient_invocations(state);
		if constexpr (!std::is_same_v<result_type, storage_type>) {
			return get_self().finalise_result(state);
		} else {
			return state.value;
		}
	}

	result_type get(state&& state) const {
		verify_sufficient_invocations(state);
		if constexpr (!std::is_same_v<result_type, storage_type>) {
			return get_self().finalise_result(std::move(state));
		} else {
			return std::move(state.value);
		}
	}

	void parse(std::vector<std::string> args, state& state) const {
		++state.invocations;
		if (state.invocations > m_allowed_invocations.max) {
			throw bad_argument{
			        "Too many arguments of type " + long_name_as_string() +
			        ". Maximum allowed: " + std::to_string(m_allowed_invocations.max)};
		}
		get_self().parse_argument(std::move(args), state);
	}

	static constexpr auto long_name() { return impl::extract_long_name<Name>(); }
	static constexpr auto short_name() { return impl::extract_short_name<Name>(); }
	static_assert(uct::string_width(short_name().to_string()) <= 2);
	static std::string long_name_as_string() { return long_name().to_string(); }

	unsigned arg_count() const { return m_allowed_arguments; }
	const std::vector<std::string>& dependencies() const { return m_dependencies; }
	const std::vector<std::string>& conflicts() const { return m_conflicts; }

	option_info info() const {
		return {short_name().to_string(),
		        long_name_as_string(),
		        get_self().argument_type_as_string(),
		        impl::value_to_string(m_initial_state.value, 0),
		        m_allowed_invocations.min > 0,
			get_self().always_available(),
		        m_desc};
	}

	static constexpr bool is_positional_argument() { return false; }

	static constexpr bool has_callback() { return false; }

	static constexpr bool always_available() { return false; }

	// These are intended to be proteced, but for some reason CRTP doesn't count as regular
	// inheritance:
	option_base<Name, Self, Result, Storage_Type>& get_base() & { return *this; }

	const option_base<Name, Self, Result, Storage_Type>& get_base() const& { return *this; }

	option_base<Name, Self, Result, Storage_Type>&& get_base() && { return *this; }

protected:
	std::string argument_type_as_string() const {
		using impl::type_to_string;
		return type_to_string(parse_type<result_type>{}, 0);
	}

private:
	void setup(invocation_range invocations) { m_allowed_invocations = invocations; }
	void setup(argument_count arg_count) { m_allowed_arguments = arg_count.n; }
	void setup(description desc) { m_desc = desc.desc; }
	void setup(struct dependencies deps) {
		m_dependencies.insert(m_dependencies.end(), deps.deps.begin(), deps.deps.end());
	}
	void setup(struct conflicts cs) {
		m_conflicts.insert(m_conflicts.end(), cs.conflicts.begin(), cs.conflicts.end());
	}
	Self& get_self() & { return static_cast<Self&>(*this); }
	const Self& get_self() const& { return static_cast<const Self&>(*this); }
	Self&& get_self() && { return static_cast<Self&&>(*this); }

	void verify_sufficient_invocations(const state& state) const {
		if (state.invocations < m_allowed_invocations.min) {
			throw bad_argument{"Insufficient instances of " + long_name_as_string() +
			                   " provided. Minimum necessary: " +
			                   std::to_string(m_allowed_invocations.min)};
		}
	}

	std::string m_desc;
	state m_initial_state;
	invocation_range m_allowed_invocations = {};
	unsigned m_allowed_arguments = 1u;
	std::vector<std::string> m_dependencies;
	std::vector<std::string> m_conflicts;
};
} // namespace clp
#endif

