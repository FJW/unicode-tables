#ifndef CLP_CALLBACK_SUPPORT_HPP
#define CLP_CALLBACK_SUPPORT_HPP

#include "clp/fixed_string.hpp"
#include <tuple>
#include <vector>

namespace clp {

template <impl::fixed_string Name>
struct arg_name {

	template <typename Results>
	static auto get(const Results& results) {
		return results.template get<Name>();
	}

	static std::string dependencies() { return Name.to_string(); }
};

template <typename T>
struct arg_value {
	T value;

	explicit arg_value(T arg): value{std::move(arg)} {}

	template <typename Results>
	const auto& get(const Results&) const {
		return value;
	}

	static std::string dependencies() { return {}; }
};

template <typename... ArgMetaTypes>
class args {
public:
	args(ArgMetaTypes... arguments) : arguments{std::move(arguments)...} {}

	std::tuple<ArgMetaTypes...> arguments;

	template <typename Function, typename Results>
	auto call(Function function, const Results& results) const {
		return call_impl(function, results,
		                 std::make_index_sequence<sizeof...(ArgMetaTypes)>{});
	}

	static auto dependencies() {
		auto ret = std::vector<std::string>{ArgMetaTypes::dependencies()...};
		std::erase(ret, "");
		return ret;
	}

private:
	template <typename Function, typename Results, std::size_t... Indices>
	auto call_impl(Function& function, const Results& results,
	               std::index_sequence<Indices...>) const {
		return function(std::get<Indices>(arguments).get(results)...);
	}

};

} // namespace clp
#endif
