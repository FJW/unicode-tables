#ifndef CLP_OPTIONS_HPP
#define CLP_OPTIONS_HPP

#include <cassert>
#include <charconv>
#include <optional>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

#include "callback_support.hpp"
#include "core.hpp"
#include "fixed_string.hpp"
#include "type_support.hpp"

namespace clp {

template <impl::fixed_string Name, typename T>
class required_argument
        : public option_base<Name, required_argument<Name, T>, T, std::optional<T>> {
public:
	using base = option_base<Name, required_argument<Name, T>, T, std::optional<T>>;
	using base::get_base;
	using state = typename base::state;

	template <typename... Args>
	required_argument(Args&&... args)
	        : base{{}, invocation_range{1, 1}, std::forward<Args>(args)...} {}

	T finalise_result(state s) const {
		assert(s.value);
		return s.value.value();
	}
	T finalise_result(state&& s) const {
		assert(s.value);
		return std::move(s.value.value());
	}

	void parse_argument(std::vector<std::string> args, state& s) const {
		assert(args.size() == 1u);
		using impl::parse_argument;
		s.value = parse_argument(parse_type<T>{}, std::move(args.front()),
		                         this->long_name_as_string(), 0);
	}
};

template <impl::fixed_string Name, typename T>
class optional_argument
        : public option_base<Name, optional_argument<Name, T>, std::optional<T>, std::optional<T>> {
public:
	using base =
	        option_base<Name, optional_argument<Name, T>, std::optional<T>, std::optional<T>>;
	using base::get_base;
	using state = typename base::state;

	template <typename... Args>
	optional_argument(Args&&... args) : base{{}, std::forward<Args>(args)...} {}

	void parse_argument(std::vector<std::string> args, state& s) const {
		assert(args.size() == 1u);
		using impl::parse_argument;
		s.value = parse_argument(parse_type<T>{}, std::move(args.front()),
		                         this->long_name_as_string(), 0);
	}
};

template <impl::fixed_string Name, typename T>
class defaulted_argument : public option_base<Name, defaulted_argument<Name, T>, T, T> {
public:
	using base = option_base<Name, defaulted_argument<Name, T>, T, T>;
	using base::get_base;
	using state = typename base::state;

	template <typename Default, typename... Args>
	defaulted_argument(Default&& default_value, Args&&... args)
	        : base{{std::forward<Default>(default_value)}, std::forward<Args>(args)...} {}

	void parse_argument(std::vector<std::string> args, state& s) const {
		assert(args.size() == 1u);
		using impl::parse_argument;
		s.value = parse_argument(parse_type<T>{}, std::move(args.front()),
		                         this->long_name_as_string(), 0);
	}
	static constexpr bool always_available() { return true; }
};

template <impl::fixed_string Name>
class bool_flag : public option_base<Name, bool_flag<Name>, bool, bool> {
public:
	using base = option_base<Name, bool_flag<Name>, bool, bool>;
	using base::get_base;
	using state = typename base::state;

	template <typename... Args>
	bool_flag(Args&&... args) : base{{}, argument_count{0}, std::forward<Args>(args)...} {}

	std::string argument_type_as_string() const { return "flag"; }

	void parse_argument(std::vector<std::string>, state& s) const { s.value = true; }
	static constexpr bool always_available() { return true; }
};

template <impl::fixed_string Name>
class bool_counter : public option_base<Name, bool_counter<Name>, unsigned, unsigned> {
public:
	using base = option_base<Name, bool_counter<Name>, unsigned, unsigned>;
	using base::get_base;
	using state = typename base::state;
	template <typename... Args>
	bool_counter(Args&&... args)
	        : base{{},
	               invocation_range{0, unlimited_invocations},
	               argument_count{0},
	               std::forward<Args>(args)...} {}

	std::string argument_type_as_string() const { return "flag..."; }

	void parse_argument(std::vector<std::string>, state& s) const { ++s.value; }
	static constexpr bool always_available() { return true; }
};

template <impl::fixed_string Name, typename T>
class collected_argument
        : public option_base<Name, collected_argument<Name, T>, std::vector<T>, std::vector<T>> {
public:
	using base = option_base<Name, collected_argument<Name, T>, std::vector<T>, std::vector<T>>;
	using base::get_base;
	using state = typename base::state;

	template <typename... Args>
	collected_argument(Args&&... args)
	        : base{{},
	               invocation_range{0, unlimited_invocations},
	               std::forward<Args>(args)...} {}

	std::string argument_type_as_string() const {
		using impl::type_to_string;
		return type_to_string(parse_type<T>{}, 0) + "...";
	}

	void parse_argument(std::vector<std::string> args, state& s) const {
		assert(args.size() == 1u);
		using impl::parse_argument;
		s.value.emplace_back(parse_argument(parse_type<T>{}, std::move(args.front()),
		                                    this->long_name_as_string(), 0));
	}
};

template <impl::fixed_string Name, typename T>
class required_positional_argument
        : public option_base<Name, required_positional_argument<Name, T>, T, std::optional<T>> {
public:
	using base = option_base<Name, required_positional_argument<Name, T>, T, std::optional<T>>;
	using base::get_base;
	using state = typename base::state;

	template <typename... Args>
	required_positional_argument(Args&&... args)
	        : base{{}, invocation_range{1, 1}, std::forward<Args>(args)...} {}

	T finalise_result(state s) const {
		assert(s.value);
		return s.value.value();
	}
	T finalise_result(state&& s) const {
		assert(s.value);
		return std::move(s.value.value());
	}

	void parse_argument(std::vector<std::string> args, state& s) const {
		assert(args.size() == 1u);
		using impl::parse_argument;
		s.value = parse_argument(parse_type<T>{}, std::move(args.front()),
		                         this->long_name_as_string(), 0);
	}

	static constexpr bool is_positional_argument() { return true; }
};

template <impl::fixed_string Name, typename Function, typename ArgList = args<>>
class callback_argument
        : public option_base<Name, callback_argument<Name, Function, ArgList>, bool, bool> {
public:
	using base = option_base<Name, callback_argument<Name, Function, ArgList>, bool, bool>;
	using base::get_base;
	using state = typename base::state;

	template <typename... CtorArgs>
	callback_argument(Function callback, ArgList arglist = {},
	                  CtorArgs&&... args)
	        : base{{}, argument_count{0}, std::forward<CtorArgs>(args)...
			, dependencies{ArgList::dependencies()} // TODO: optional arguments should be treated as passed.
			}, arglist{arglist},
	          callback{std::move(callback)} {}

	static constexpr bool has_callback() { return true; }

	template <typename Results>
	auto execute_callback(const Results& results) const {
		return arglist.call(callback, results);
	}

	void parse_argument(std::vector<std::string>, state& s) const { s.value = true; }
	std::string argument_type_as_string() const { return "flag"; }

private:
	ArgList arglist;
	Function callback;
};

} // namespace clp

#endif
