#ifndef CLP_CORE_HPP
#define CLP_CORE_HPP

#include <algorithm>
#include <format>
#include <functional>
#include <memory>
#include <numeric>
#include <ostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <uct/graphemes.hpp>
#include <uct/width.hpp>

#include "fixed_string.hpp"
#include "type_support.hpp"
#include "dependency_checker.hpp"

#include "option_base.hpp"

namespace clp {


namespace impl {
inline std::pair<std::vector<std::string>, std::ptrdiff_t>
take_n_args(std::vector<std::string>::const_iterator it, const std::vector<std::string>& args,
            std::size_t n, std::string_view possible_arg, std::string_view parameter_name) {
	const auto remaining_elements = static_cast<std::size_t>(std::distance(it, args.end()));
	auto ret = std::vector<std::string>{};
	if (not possible_arg.empty()) {
		ret.emplace_back(possible_arg);
		--n;
	}
	if (n > remaining_elements) {
		throw bad_argument{"not enough arguments for paramter " +
		                   std::string{parameter_name}};
	}
	std::copy_n(it, n, std::back_inserter(ret));
	return {ret, static_cast<std::ptrdiff_t>(n)};
}
} // namespace impl

template <impl::string_list Typenames, typename... Values>
struct result {
	std::tuple<Values...> values;
	template <impl::fixed_string S>
	auto get() const {
		return impl::get_value<S>(Typenames, values);
	}
	template <std::size_t I>
	auto get() const {
		return std::get<I>(values);
	}
};
} // namespace clp

namespace std {
template <::clp::impl::string_list S, class... Types>
struct tuple_size<::clp::result<S, Types...>> : integral_constant<size_t, sizeof...(Types)> {};
template <size_t I, clp::impl::string_list S, class... Types>
struct tuple_element<I, ::clp::result<S, Types...>> : tuple_element<I, tuple<Types...>> {};
} // namespace std

namespace clp {
template <typename... Parameters>
class arg_parser {
public:
	arg_parser(Parameters... params) : m_params{std::move(params)...} {
		setup_parameters(indeces{});
	}

	using long_names = impl::string_list<Parameters::long_name()...>;
	static_assert(not impl::contains_duplicates(long_names{}));
	using names = impl::string_list<Parameters::name()...>;
	static_assert(not impl::contains_non_empty_duplicates(names{}));
	using short_names = impl::string_list<Parameters::short_name()...>;
	static_assert(not impl::contains_non_empty_duplicates(short_names{}));

	using result_type = result<names{}, typename Parameters::result_type...>;
	using storage_type = std::tuple<parsing_state<typename Parameters::storage_type>...>;

	storage_type gen_parsing_state() const { return gen_parsing_state_impl(indeces{}); }

	result_type parse(const std::vector<std::string>& args) const {
		auto storage = gen_parsing_state();
		auto dep_checker = impl::dependency_checker{};
		auto next_positional_index = std::size_t{};

		for(const auto& option_info: get_information_vector(indeces{})) {
			if (option_info.always_available) {
				dep_checker.always_available(option_info.long_name);
			}
		}

		for (auto it = args.begin(); it != args.end();) {
			if (it->empty() or *it == "-") {
				throw bad_argument{"invalid argument"};
			} else if (it->front() != '-') {
				if (m_positional_args.size() <= next_positional_index) {
					throw bad_argument{"too many positional arguments"};
				}
				// positional argument
				auto [opt_args, n] = impl::take_n_args(
				        it, args,
				        m_positional_args[next_positional_index].argument_count, "",
				        "");
				m_positional_args[next_positional_index++].parser(
				        std::move(opt_args), storage);
				it += n;
			} else {
				auto [name, data_it, first_arg] = next_option(*it);
				++it;
				auto [opt_args, n] = impl::take_n_args(
				        it, args, data_it->second.argument_count, first_arg, name);
				it += n;
				data_it->second.parser(std::move(opt_args), storage);
				dep_checker.add(name, data_it->second.dependencies, data_it->second.conflicts);
			}
		}
		dep_checker.check_all();
		const auto results = return_results(indeces{}, std::move(storage));
		execute_callbacks(std::make_index_sequence<sizeof...(Parameters)>{}, results);
		return results;
	}

	result_type parse(int argc, const char** argv) const {
		return parse({argv + 1, argv + argc});
	}

	void print_help(std::string_view program_name, std::ostream& s) const {
		const auto information = get_information_vector(indeces{});
		struct lengths_t {
			std::size_t short_name_length = 0;
			std::size_t long_name_length = 0;
			std::size_t typename_length = 0;
			std::size_t default_value_length = 0;
		};
		const auto [short_name_length, long_name_length, typename_length,
		            default_value_length] =
		        std::accumulate(
		                information.begin(), information.end(), lengths_t{},
		                [](lengths_t l, const option_info& i) {
			                return lengths_t{
			                        std::max(l.short_name_length,
			                                 uct::string_width(i.short_name)),
			                        std::max(l.long_name_length,
			                                 uct::string_width(i.long_name)),
			                        std::max(l.typename_length,
			                                 uct::string_width(i.argument_type)),
			                        std::max(l.default_value_length,
			                                 uct::string_width(i.default_argument))};
		                });
		const auto fill = [](std::size_t i) { return std::string(i, ' '); };

		s << "Usage: " << program_name << " [options]\n";
		for (const auto& info : information) {
			s << "  ";
			if (auto c = info.short_name; not c.empty()) {
				s << '-' << c << ", "
				  << std::string(short_name_length - uct::string_width(c), ' ');
			} else {
				s << "   " << std::string(short_name_length, ' ');
			}
			assert(not info.argument_type.empty());
			const auto arg_type = info.required ? "⟨" + info.argument_type + "⟩"
			                                    : '[' + info.argument_type + ']';
			s << "--" << info.long_name
			  << fill(long_name_length - uct::string_width(info.long_name))
			  << (info.argument_type.empty() ? "   " : " : ") << arg_type
			  << fill(typename_length - uct::string_width(info.argument_type))
			  << (info.default_argument.empty() ? "   " : " = ")
			  << info.default_argument
			  << fill(default_value_length - uct::string_width(info.default_argument))
			  << " " << info.description << '\n';
		}
	}

	std::string generate_help(std::string_view program_name) const {
		auto stream = std::ostringstream{};
		print_help(program_name, stream);
		return stream.str();
	}

private:
	using indeces = std::make_index_sequence<sizeof...(Parameters)>;

	struct option_data {
		std::function<void(std::vector<std::string>, storage_type& out)> parser;
		unsigned argument_count;
		std::vector<std::string> dependencies;
		std::vector<std::string> conflicts;
	};

	template <std::size_t... I>
	storage_type gen_parsing_state_impl(std::index_sequence<I...>) const {
		return storage_type{std::get<I>(m_params).initial_state()...};
	}

	std::tuple<std::string,
	           typename std::unordered_map<std::string, option_data>::const_iterator,
	           std::string_view>
	next_option(std::string_view arg) const {
		auto option_name = std::string{}; // arg.substr(2);
		auto first_arg = std::string_view{};
		if (auto [grapheme, tail] = uct::peel_grapheme(arg.substr(1));
		    grapheme.str.at(0) != '-') { // short option
			const auto c = std::string{grapheme.str};
			const auto it = m_short_long_map.find(c);
			if (it == m_short_long_map.end()) {
				throw bad_argument{
				        std::format("unknown short option: “{}”", grapheme.str)};
			}
			option_name = it->second;
			if (not tail.empty()) {
				first_arg = tail;
			}
		} else { // long option
			const auto i = arg.find('=', 2);
			if (i == std::string::npos) {
				option_name = arg.substr(2);
			} else {
				option_name = arg.substr(2, i - 2);
				first_arg = arg;
				first_arg.remove_prefix(i + 1);
			}
		}
		const auto it = m_adders.find(option_name);
		if (it == m_adders.end()) {
			throw bad_argument{std::format("unknown option: “{}”", arg)};
		}
		return {option_name, it, first_arg};
	}

	template <std::size_t... I>
	auto setup_parameters(std::index_sequence<I...>) {
		(void)std::initializer_list<int>{[&](auto& param) {
			// for some reason clang rejects param.is_positional_argument() which really
			// should work!
			constexpr auto positional =
			        std::decay_t<decltype(param)>::is_positional_argument();
			if constexpr (not positional) {
				m_adders[param.long_name_as_string()] = option_data{
				        [ptr = &param](std::vector<std::string> args,
				                       storage_type& s) {
					        ptr->parse(std::move(args), std::get<I>(s));
				        },
				        param.arg_count(), param.dependencies(), param.conflicts()};
				if (not param.short_name().empty()) {
					m_short_long_map[param.short_name().to_string()] =
					        param.long_name_as_string();
				}
			} else {
				m_positional_args.push_back(option_data{
				        [ptr = &param](std::vector<std::string> args,
				                       storage_type& s) {
					        ptr->parse(std::move(args), std::get<I>(s));
				        },
				        param.arg_count(), param.dependencies(), param.conflicts()});
			}
			return 0;
		}(std::get<I>(m_params))...};
	}

	template <std::size_t... I>
	void execute_callbacks(std::index_sequence<I...>, result_type results) const {
		(void)std::initializer_list<int>{[&](const auto& param, const auto& res) {
			if constexpr (param.has_callback()) {
				if (res) {
					param.execute_callback(results);
				}
			}
			return 0;
		}(std::get<I>(m_params), (results.template get<I>()))...};
	}

	template <std::size_t... I>
	result_type return_results(std::index_sequence<I...>, storage_type storage) const {
		return result_type{{(std::get<I>(m_params).get(std::get<I>(storage)))...}};
	}

	template <std::size_t... I>
	std::vector<option_info> get_information_vector(std::index_sequence<I...>) const {
		return {(std::get<I>(m_params).info())...};
	}

	std::tuple<Parameters...> m_params;
	std::unordered_map<std::string, option_data> m_adders;
	std::unordered_map<std::string, std::string> m_short_long_map;
	std::vector<option_data> m_positional_args;
};

template <typename... Params>
inline auto parse(std::vector<std::string> args, Params... params) {
	auto parser = arg_parser{std::move(params)...};
	return parser.parse(std::move(args));
}

template <typename... Params>
inline auto parse(int argc, char** argv, Params... params) {
	return parse(std::vector<std::string>{argv + 1, argv + argc}, std::move(params)...);
}

} // namespace clp

#endif
