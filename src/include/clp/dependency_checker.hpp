#ifndef CLP_DEPENDENCY_CHECKER_HPP
#define CLP_DEPENDENCY_CHECKER_HPP

#include <stdexcept>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <uct/string_utils.hpp>

#include <clp/type_support.hpp>

namespace clp {
struct dependencies {
	std::vector<std::string> deps;
};

struct conflicts {
	std::vector<std::string> conflicts;
};
} // namespace clp

namespace clp::impl {
class dependency_checker {
public:
	dependency_checker() {}

	template <std::ranges::range Dependencies,
	          std::ranges::range Conflicts = std::vector<std::string>>
	void add(std::string name, const Dependencies& deps, Conflicts conflicts = {}) {
		m_observed[name] = observed::provided;
		for (const auto& dep : deps) {
			m_dependencies[dep].insert(name);
		}
		for (const auto& conf : conflicts) {
			m_conflicts[conf].insert(name);
		}
	}

	void always_available(const std::string& name) {
		m_observed[name] = observed::always_available;
	}

	void check_all() {
		auto missing = std::vector<std::pair<std::string, std::vector<std::string>>>{};
		for (const auto& [dep, sources] : m_dependencies) {
			if (m_observed[dep] == observed::missing) {
				missing.emplace_back(dep,
				                     std::vector(sources.begin(), sources.end()));
			}
		}
		auto conflicts = std::vector<std::pair<std::string, std::vector<std::string>>>{};
		for (const auto& [conflict, sources] : m_conflicts) {
			if (m_observed[conflict] == observed::provided) {
				conflicts.emplace_back(conflict,
				                       std::vector(sources.begin(), sources.end()));
			}
		}

		if (missing.empty() and conflicts.empty()) {
			return;
		}
		auto ret_str = std::string{};
		if (not missing.empty()) {
			ret_str += "The following arguments are required dependencies that were "
			           "not provided:\n";
			for (const auto& [arg, deps] : missing) {
				ret_str += "∙ “" + arg + "” is required by: “" +
				           uct::join_strings("”, “", deps) + "”\n";
			}
		}
		if (not conflicts.empty()) {
			ret_str += "The following arguments are in conflict with other arguments "
			           "that were passed:\n";
			for (const auto& [arg, conflicting_args] : conflicts) {
				ret_str += "∙ “" + arg + "” is conflicting with: “" +
				           uct::join_strings("”, “", conflicting_args) + "”\n";
			}
		}
		throw clp::bad_argument{ret_str};
	}

private:
	enum class observed { missing, always_available, provided };
	std::unordered_map<std::string, std::unordered_set<std::string>> m_dependencies;
	std::unordered_map<std::string, std::unordered_set<std::string>> m_conflicts;
	std::unordered_map<std::string, observed> m_observed;
};
} // namespace clp::impl

#endif
