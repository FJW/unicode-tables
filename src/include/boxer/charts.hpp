#ifndef BOXER_CHARTS_HPP
#define BOXER_CHARTS_HPP

#include <algorithm>
#include <ranges>
#include <span>

#include <uct/string_utils.hpp>
#include <uct/width.hpp>

#include <boxer/boxer.hpp>

namespace boxer {

template <std::ranges::forward_range Container, std::ranges::forward_range Labels>
void print_chart(box& box, const Container& data, const Labels& labels) {
	const auto ends = std::array<std::string, 8>{"", "▏", "▎", "▍", "▌", "▋", "▊", "▉"};
	if (data.empty()) {
		return;
	}
	const auto max = *std::max_element(data.begin(), data.end());
	const auto data_width = std::formatted_size("{}", max);
	const auto max_label =
	        std::ranges::max_element(labels, {}, [](auto& s) { return uct::string_width(s); });
	const auto label_width = uct::string_width(*max_label);
	const auto width = 8 * (box.text_width() - label_width - 2 - data_width);
	// for (auto&& [l, x]: std::ranges::zip(labels, data)) { // zip is not yet implemented 😞︎
	auto label_it = labels.begin();
	const auto labels_end = labels.end();
	const auto empty_string = std::string{};
	for (auto&& x : data) {
		const auto relative_width = x * width / max;
		const auto& l = label_it != labels_end ? *label_it++ : empty_string;
		const auto lw = uct::string_width(l);
		const auto bar_length = relative_width / 8u;
		box.println(
		        "{}{}│{}{}{}│{:{}}", l, std::string(label_width - lw, ' '),
		        uct::multiply_string("█", bar_length), ends[relative_width % 8],
		        std::string(width / 8 - bar_length - ((relative_width % 8) ? 1 : 0), ' '),
		        x, data_width);
	}
}

template <std::ranges::forward_range Container, std::ranges::forward_range Labels>
void print_chart(const Container& data, const Labels& labels, std::string_view top_label) {
	auto temp_box = box{top_label};
	print_chart(temp_box, data, labels);
}

template <std::ranges::forward_range Container>
void print_chart(box& box, const Container& data, std::string_view top_label) {
	print_chart(box, data | std::ranges::views::values, data | std::ranges::views::keys,
	            top_label);
}

template <std::ranges::forward_range Container>
void print_chart(const Container& data, std::string_view top_label) {
	print_chart(data | std::ranges::views::values, data | std::ranges::views::keys, top_label);
}

template <std::ranges::forward_range Container, std::ranges::forward_range Labels>
void print_vertical_chart(box& box, const Container& data, const Labels& labels,
                          unsigned height = 50u, unsigned separation_width = 1,
                          std::format_string<double> format = "{: 10.0f}") {
	const auto partial_boxes =
	        std::array<std::string, 8>{" ", "▁", "▂", "▃", "▄", "▅", "▆", "▇"};
	if (data.empty()) {
		return;
	}
	const auto max = *std::max_element(data.begin(), data.end());
	const auto data_width = std::formatted_size(format, 1.0 * max);
	const auto unit_height = height * 8.0 / max;
	const auto bar_width =
	        (box.text_width() - std::size(data) * separation_width - data_width - 3) /
	        std::size(data);
	if (bar_width == 0) {
		throw std::runtime_error{"Chart contains to many datapoints to print on terminal"};
	}
	auto heights = std::vector<unsigned>{};
	for (const auto& datum : data) {
		heights.push_back(static_cast<unsigned>(datum * unit_height + 0.5));
	}
	auto line = std::string{};
	for (auto h = height; h-- > 0;) {
		line = std::format(format, (h * 8 + 4) / unit_height) + " ┤";
		for (auto bar : heights) {
			line += std::string(separation_width, ' ');
			const auto full_blocks = bar / 8u;
			const auto partial_blocks = bar % 8u;
			if (h < full_blocks) {
				line += uct::multiply_string("█", bar_width);
			} else if (h == full_blocks) {
				line += uct::multiply_string(partial_boxes[partial_blocks],
				                             bar_width);
			} else {
				line += uct::multiply_string(" ", bar_width);
			}
		}
		box.println("{}", line);
	}
	line = std::string(data_width + 2, ' ');
	for (const auto& label : labels) {
		const auto label_width = uct::string_width(label);
		if (label_width > bar_width) {
			throw std::runtime_error{std::format(
			        "Label “{}” to long ({} characters) to fit under bar of width {}",
			        label, label_width, bar_width)};
		}
		const auto pad_len = bar_width - label_width;
		const auto l_pad_len = pad_len / 2u;
		const auto r_pad_len = pad_len - l_pad_len;
		line += std::string(l_pad_len + separation_width, ' ');
		line += label;
		line += std::string(r_pad_len, ' ');
	}
	box.println("{}", line);
}

template <std::ranges::forward_range Container, std::ranges::forward_range Labels>
void print_vertical_chart(const Container& data, const Labels& labels, std::string_view top_label) {
	auto temp_box = box{top_label};
	print_vertical_chart(temp_box, data, labels);
}

template <std::ranges::forward_range Container>
void print_vertical_chart(box& box, const Container& data, std::string_view top_label) {
	print_vertical_chart(box, data | std::ranges::views::values,
	                     data | std::ranges::views::keys, top_label);
}

template <std::ranges::forward_range Container>
void print_vertical_chart(const Container& data, std::string_view top_label) {
	print_vertical_chart(data | std::ranges::views::values, data | std::ranges::views::keys,
	                     top_label);
}

} // namespace boxer

#endif // BOXER_CHARTS_HPP
