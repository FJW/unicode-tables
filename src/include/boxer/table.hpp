#ifndef BOXER_TABLE_HPP
#define BOXER_TABLE_HPP

#include <cstdint>
#include <functional>
#include <string>
#include <vector>

#include <uct/width.hpp>

namespace boxer {

struct matrix_style {
	std::string seperator;
	std::string left;
	std::string left_top;
	std::string left_bottom;
	std::string right;
	std::string right_top;
	std::string right_bottom;
};

std::string to_table(const std::vector<std::vector<std::string>>& data,
                     const std::string_view sep = " ", const std::string_view outer = "");
std::string to_table(const std::vector<std::vector<std::string>>& data,
                     const std::function<std::string(int, std::size_t)> seps);

std::string to_matrix(const std::vector<std::vector<std::string>>& data, const matrix_style& style);

namespace styles {
const auto paren_matrix = matrix_style{"  ", "⎜", "⎛", "⎝", "⎟", "⎞", "⎠"};
const auto bracket_matrix = matrix_style{"  ", "⎢", "⎡", "⎣", "⎥", "⎤", "⎦"};
} // namespace styles

} // namespace boxer

#endif // BOXER_TABLE_HPP
