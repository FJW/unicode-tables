#ifndef BOXER_TERMINAL_DIMENSIONS_HPP
#define BOXER_TERMINAL_DIMENSIONS_HPP


namespace boxer {

struct terminal_dimensions {
	unsigned width;
	unsigned height;
};

terminal_dimensions get_terminal_dimensions();

} // namespace boxer

#endif
