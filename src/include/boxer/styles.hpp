#ifndef BOXER_STYLES_HPP
#define BOXER_STYLES_HPP

#include <cstdint>
#include <stdexcept>
#include <string>
#include <vector>
#include <format>

// See https://en.wikipedia.org/wiki/ANSI_escape_code for more info

namespace boxer {

enum class color : std::uint8_t {
	black = 30,
	red,
	green,
	yellow,
	blue,
	magenta,
	cyan,
	white,
	direct = 38,
	default_color = 39,
	bright_black = 90,
	gray = bright_black,
	bright_red,
	bright_green,
	bright_yellow,
	bright_blue,
	bright_magenta,
	bright_cyan,
	bright_white,
	none = 255, // extension for this library
};

struct rgb_color{
	std::uint8_t red = 0;
	std::uint8_t green = 0;
	std::uint8_t blue = 0;
};

struct forground_color {
	color col = color::none;
	rgb_color rgb = {};
};

inline constexpr forground_color fg(color c) { return {c}; }
inline constexpr forground_color fg(rgb_color c) { return {color::direct, c}; }

struct background_color {
	color col = color::none;
	rgb_color rgb = {};
};

inline constexpr background_color bg(color c) { return {c}; }
inline constexpr background_color bg(rgb_color c) { return {color::direct, c}; }

enum class font_style : std::uint8_t {
	reset,
	bold,
	faint,
	italic,
	underlined,
	slow_blink,
	rapid_blink,
	invert,
	conceal,
	strike,
	// default_font,
	double_underlined = 21,
	normal_intensity = 22,
	not_italic,
	not_underlined,
	not_blinking,
	reveal = 28,
	not_strike
};

constexpr inline font_style operator!(font_style s) {
	switch (s) {
	case font_style::bold:
		return font_style::normal_intensity;
	case font_style::faint:
		return font_style::normal_intensity;
	case font_style::italic:
		return font_style::not_italic;
	case font_style::not_italic:
		return font_style::italic;
	case font_style::underlined:
		return font_style::not_underlined;
	case font_style::not_underlined:
		return font_style::underlined;
	case font_style::strike:
		return font_style::not_strike;
	case font_style::not_strike:
		return font_style::strike;
	case font_style::slow_blink:
		return font_style::not_blinking;
	case font_style::rapid_blink:
		return font_style::not_blinking;
	case font_style::conceal:
		return font_style::reveal;
	case font_style::reveal:
		return font_style::conceal;
	case font_style::invert:
		return font_style::invert;
	default:
		throw std::runtime_error("style cannot be inverted");
	}
}

struct text_style {
	// intentionally implicit:
	constexpr text_style() {}
	constexpr text_style(forground_color fg) : fg{fg.col} {}
	constexpr text_style(forground_color fg, background_color bg) : fg{fg.col}, bg{bg.col} {}
	constexpr text_style(background_color bg) : bg{bg.col} {}
	constexpr text_style(font_style fs); // implemented below via |=

	forground_color fg = {};
	background_color bg = {};
	forground_color prev_fg = {};
	background_color prev_bg = {};
	bool bold : 1 = false;
	bool faint : 1 = false;
	bool italic : 1 = false;
	bool underline : 1 = false;
	bool slow_blink : 1 = false;
	bool rapid_blink : 1 = false;
	bool invert : 1 = false;
	bool conceal : 1 = false;
	bool strike : 1 = false;
};

static_assert(sizeof(text_style) <= 32);

enum class style_change : std::uint8_t {
	no_change,
	enable,
	disable,
};

struct style_update {
	// intentionally implicit:
	constexpr style_update() {}
	constexpr style_update(forground_color fg) : fg{fg} {}
	constexpr style_update(forground_color fg, background_color bg) : fg{fg}, bg{bg} {}
	constexpr style_update(background_color bg) : bg{bg} {}
	constexpr style_update(font_style fs); // implemented below via |=

	forground_color fg = {};
	background_color bg = {};
	forground_color prev_fg = {};
	background_color prev_bg = {};
	style_change bold : 2 = style_change::no_change;
	style_change faint : 2 = style_change::no_change;
	style_change italic : 2 = style_change::no_change;
	style_change underline : 2 = style_change::no_change;
	style_change slow_blink : 2 = style_change::no_change;
	style_change rapid_blink : 2 = style_change::no_change;
	style_change invert : 2 = style_change::no_change;
	style_change conceal : 2 = style_change::no_change;
	style_change strike : 2 = style_change::no_change;
	bool inherit : 1 = true;
};

static_assert(sizeof(style_update) <= 32);

constexpr inline style_update operator|(forground_color fg, background_color bg) {
	return style_update{fg, bg};
}

constexpr inline style_update& operator|=(style_update& s, font_style f) {
	switch (f) {
	case font_style::bold:
		s.bold = style_change::enable;
		s.faint = style_change::disable;
		break;
	case font_style::faint:
		s.faint = style_change::enable;
		s.bold = style_change::disable;
		break;
	case font_style::italic:
		s.italic = style_change::enable;
		break;
	case font_style::not_italic:
		s.italic = style_change::disable;
		break;
	case font_style::underlined:
		s.underline = style_change::enable;
		break;
	case font_style::not_underlined:
		s.underline = style_change::disable;
		break;
	case font_style::strike:
		s.strike = style_change::enable;
		break;
	case font_style::not_strike:
		s.strike = style_change::disable;
		break;
	case font_style::slow_blink:
		s.slow_blink = style_change::enable;
		break;
	case font_style::rapid_blink:
		s.rapid_blink = style_change::enable;
		break;
	case font_style::conceal:
		s.conceal = style_change::enable;
		break;
	case font_style::reveal:
		s.conceal = style_change::disable;
		break;
	case font_style::invert:
		s.invert = style_change::enable;
		break;
	case font_style::reset:
		s.inherit = false;
		break;
	case font_style::normal_intensity:
		s.bold = style_change::disable;
		s.faint = style_change::disable;
		break;
	default:
		throw std::runtime_error("style not supported");
	}
	return s;
}

constexpr inline text_style operator|(text_style s, style_update su) {
	auto ret = s;
	if (not su.inherit) {
		ret = text_style{};
	}
	if (su.fg.col != color::none) {
		ret.fg =su.fg;
	}
	if (su.bg.col != color::none) {
		ret.bg =su.bg;
	}
	auto compute = [](bool old, style_change new_) {
		switch (new_) {
		case style_change::enable:
			return true;
		case style_change::disable:
			return false;
		case style_change::no_change:
			return old;
		}
		throw std::runtime_error("bad style_change");
	};
	ret.bold = compute(ret.bold, su.bold);
	ret.italic = compute(ret.italic, su.italic);
	ret.faint = compute(ret.faint, su.faint);
	ret.underline = compute(ret.underline, su.underline);
	ret.slow_blink = compute(ret.slow_blink, su.slow_blink);
	ret.rapid_blink = compute(ret.rapid_blink, su.rapid_blink);
	ret.invert = compute(ret.invert, su.invert);
	ret.conceal = compute(ret.conceal, su.conceal);
	ret.strike = compute(ret.strike, su.strike);
	return ret;
}

constexpr inline text_style operator|=(text_style& s, style_update su) { return s = s | su; }

constexpr inline text_style& operator|=(text_style& s, font_style f) {
	switch (f) {
	case font_style::bold:
		s.bold = true;
		s.faint= false;
		break;
	case font_style::faint:
		s.faint = true;
		s.bold = false;
		break;
	case font_style::italic:
		s.italic = true;
		break;
	case font_style::not_italic:
		s.italic = false;
		break;
	case font_style::underlined:
		s.underline = true;
		break;
	case font_style::not_underlined:
		s.underline = false;
		break;
	case font_style::strike:
		s.strike = true;
		break;
	case font_style::not_strike:
		s.strike = false;
		break;
	case font_style::slow_blink:
		s.slow_blink = true;
		break;
	case font_style::rapid_blink:
		s.rapid_blink = true;
		break;
	case font_style::conceal:
		s.conceal = true;
		break;
	case font_style::reveal:
		s.conceal = false;
		break;
	case font_style::invert:
		s.invert = true;
		break;
	case font_style::normal_intensity:
		s.bold = false;
		s.faint = false;
		break;
	default:
		throw std::runtime_error("style not supported");
	}
	return s;
}

constexpr style_update::style_update(font_style fs) { *this |= fs; }

constexpr inline style_update operator|(style_update su, font_style fs) { return su |= fs; }

constexpr inline style_update operator|(forground_color fg, font_style s) {
	return style_update{fg} | s;
}

constexpr inline style_update operator|(background_color bg, font_style s) {
	return style_update{bg} | s;
}

constexpr inline style_update operator|(font_style fs, forground_color fg) {
	auto ret = style_update{};
	ret |= fs;
	ret.fg = fg;
	return ret;
}

constexpr inline style_update operator|(font_style fs, background_color bg) {
	auto ret = style_update{};
	ret |= fs;
	ret.bg = bg;
	return ret;
}

constexpr inline style_update operator|(font_style s1, font_style s2) {
	return style_update{} | s1 | s2;
}

constexpr inline std::string to_escape_code(text_style s) {
	auto ret = std::string{"\033[0"};
	auto codes = std::vector<std::string>{};
	const auto style_to_str = [&](bool b, font_style fs) {
		if (b) {
			codes.push_back(std::to_string(unsigned{static_cast<std::uint8_t>(fs)}));
		}
	};
	if (s.fg.col == color::direct) {
		codes.push_back(std::format("38;2;{};{};{}",s.fg.rgb.red,s.fg.rgb.green, s.fg.rgb.blue));
	} else if (s.fg.col != color::none) {
		codes.push_back(std::to_string(unsigned{static_cast<std::uint8_t>(s.fg.col)}));
	}
	if (s.bg.col == color::direct) {
		codes.push_back(std::format("48;2;{};{};{}",s.bg.rgb.red,s.bg.rgb.green, s.bg.rgb.blue));
	} else if (s.bg.col != color::none) {
		codes.push_back(std::to_string(unsigned{static_cast<std::uint8_t>(s.bg.col) + 10u}));
	}
	style_to_str(s.bold, font_style::bold);
	style_to_str(s.faint, font_style::faint);
	style_to_str(s.italic, font_style::italic);
	style_to_str(s.underline, font_style::underlined);
	style_to_str(s.slow_blink, font_style::slow_blink);
	style_to_str(s.rapid_blink, font_style::rapid_blink);
	style_to_str(s.invert, font_style::invert);
	style_to_str(s.conceal, font_style::conceal);
	style_to_str(s.strike, font_style::strike);
	for (const auto& update : codes) {
		ret += ";";
		ret += update;
	}
	ret += "m";
	return ret;
}

constexpr inline std::string reset_code() { return "\033[0m"; }

} // namespace boxer

#endif // BOXER_STYLES_HPP

