#ifndef BOXER_HEATMAP_HPP
#define BOXER_HEATMAP_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <ranges>
#include <span>
#include <thread>

#include <uct/string_utils.hpp>
#include <uct/width.hpp>

#include <boxer/boxer.hpp>
#include <boxer/styles.hpp>
#include <boxer/threadpool.hpp>
#include <boxer/utils.hpp>
#include <boxer/terminal_dimensions.hpp>
#include <variant>

namespace boxer {

constexpr inline auto identity = [](auto&& arg) { return arg; };

inline rgb_color normal_float_to_height_color(double value) {
	if (value < 0.0) {
		return {0, 0, 255};
	} else if (value > 1.0) {
		return {255, 255, 255};
	} else {
		return {0, static_cast<std::uint8_t>(std::round(value * 255.0)), 0};
	}
}

namespace impl {

constexpr auto double_pixel_black = std::string_view{"\033[0;38;2;000;000;000;48;2;000;000;000m▀"};
constexpr auto upper_pixel_black = std::string_view{"\033[0;38;2;000;000;000                 m▀"};

constexpr auto double_pixel_bytes = double_pixel_black.size();

inline void to_double_pixel(rgb_color top, rgb_color bot, std::output_iterator<char> auto out) {
	std::format_to(out, "\033[0;38;2;{:03};{:03};{:03};48;2;{:03};{:03};{:03}m▀", top.red,
	               top.green, top.blue, bot.red, bot.green, bot.blue);
}
inline void to_double_pixel(rgb_color top, std::output_iterator<char> auto out) {
	std::format_to(out, "\033[0;38;2;{:03};{:03};{:03}                       m▀", top.red,
	               top.green, top.blue);
}

class canvas {
public:
	canvas(box& box, std::size_t width, std::size_t height) : m_width{width}, m_height{height} {
		const auto jump_start_instruction =
		        std::format("\033[{}A\033[{}C", (height + 1u) / 2, box.border_width());
		m_start_offset = jump_start_instruction.size();
		const auto line_end = std::format("\033[1B\033[{}D", width);
		m_line_end_size = line_end.size();
		m_buffer.reserve(m_start_offset +
		                 height * (width * impl::double_pixel_bytes + m_line_end_size) +
		                 4u);
		m_buffer += jump_start_instruction;
		const auto line = uct::multiply_string(impl::double_pixel_black, width) + line_end;
		for (auto i = std::size_t{}; i < height / 2u; ++i) {
			m_buffer += line;
		}
		if (height % 2u == 1) {
			m_buffer += uct::multiply_string(impl::upper_pixel_black, width) + line_end;
		}
		m_buffer += "\033[1E";
	}

	void set_pixel(std::size_t x, std::size_t y, rgb_color col) {
		assert(x < m_width);
		assert(y < m_height);
		const auto neg_y = m_height - y;
		const auto sub_pixel_offset = neg_y % 2u == 0u ? 9u : 26u;
		const auto pos =
		        m_start_offset +
		        neg_y / 2u * (m_width * impl::double_pixel_bytes + m_line_end_size) +
		        x * impl::double_pixel_bytes + sub_pixel_offset;
		std::format_to(m_buffer.begin() + static_cast<std::ptrdiff_t>(pos),
		               "{:03};{:03};{:03}", col.red, col.green, col.blue);
	}

	std::string_view buffer() const& { return m_buffer; }
	std::string_view buffer() const&& = delete;
	std::string_view buffer() && = delete;

private:
	std::size_t m_width;
	std::size_t m_height;
	std::size_t m_start_offset;
	std::size_t m_line_end_size;
	std::string m_buffer;
};
} // namespace impl

struct coordinate {
	std::size_t x;
	std::size_t x_max;
	std::size_t y;
	std::size_t y_max;
	std::size_t z = 0u;
	std::size_t z_max = 1u;
};

void print_heatmap(box& box, std::size_t width = 0u, std::size_t height = 80u,
                   std::size_t z_max_passthrough = 1u, auto... shaders) {
	const auto text_width = box.text_width();
	if (width > text_width) {
		throw boxer_error{"requested width of heatmap wider than display-area"};
	}
	width = (width != 0) ? width : text_width;
	height = (height != 0) ? height : get_terminal_dimensions().height - 2u;
	const auto padding = std::string(text_width - width, ' ');
	auto line = std::string(width * impl::double_pixel_bytes, ' ') + reset_code() + padding;
	for (auto y = height + 1u; y > 1u; y -= 2u) {
		for (auto x = std::size_t{}; x < width; ++x) {
			const auto col_top = impl::fold(shaders...)(
			        coordinate{x, width, y - 1, height, 0, z_max_passthrough});
			const auto out_it = line.begin() + static_cast<std::ptrdiff_t>(
			                                           x * impl::double_pixel_bytes);
			if (y == 2) {
				impl::to_double_pixel(col_top, out_it);
			} else {
				const auto col_bot = impl::fold(shaders...)(
				        coordinate{x, width, y - 2, height, 0, z_max_passthrough});
				impl::to_double_pixel(col_top, col_bot, out_it);
			}
		}
		box.unchecked_print_single_line(line);
	}
}

void print_pixmaps(box& box, std::size_t width = 0u, std::size_t height = 80u, std::size_t n = 1,
                   std::chrono::milliseconds delay = std::chrono::milliseconds{0},
                   auto... shaders) {
	const auto text_width = box.text_width();
	if (width > text_width) {
		throw boxer_error{"requested width of heatmap wider than display-area"};
	}
	width = (width != 0) ? width : text_width;
	height = (height != 0) ? height : 2u * get_terminal_dimensions().height - 4u;
	print_heatmap(box, width, height, n, shaders...);
	auto data = std::string{};
	const auto jump_start_instruction =
	        std::format("\033[{}A\033[{}C", (height + 1u) / 2, box.border_width());
	const auto next_line_instruction = reset_code() + std::format("\033[1B\033[{}D", width);
	const auto shader_pack = impl::fold(shaders...);
	auto canvas = impl::canvas(box, width, height);
	auto threadpool = make_static_threadpool<std::size_t>([&](std::size_t z, std::size_t y) {
		for (auto x = std::size_t{}; x < width; ++x) {
			canvas.set_pixel(x, y,
			                 shader_pack((coordinate{x, width, y - 1, height, z, n})));
		}
	});
	for (auto z = std::size_t{1}; z < n; ++z) {
		std::this_thread::sleep_for(delay);
		threadpool.run(z, height, 10);
		box.unchecked_print(canvas.buffer());
	}
}

} // namespace boxer

void temp();

#endif // BOXER_HEATMAP_HPP
