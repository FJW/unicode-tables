#ifndef BOXER_UTILS_HPP
#define BOXER_UTILS_HPP

#include <type_traits>

namespace boxer::impl {

template<typename T>
struct function_container {
	T function;
};

template<typename Lhs, typename Rhs>
auto operator|(function_container<Lhs> lhs, function_container<Rhs> rhs) {
	return function_container{[lhs, rhs](auto&&... args) {
		return rhs.function(lhs.function(std::forward<decltype(args)>(args))...);
	}};
}

template<typename... Functions>
auto fold(Functions&&... functions) {

	return [functions...](auto&&...args) {
		return (... | function_container{functions}).function(std::forward<decltype(args)>(args)...);
	};
}

} // namespace boxer::impl

#endif
