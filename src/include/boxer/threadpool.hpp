#ifndef BOXER_THREADPOOL
#define BOXER_THREADPOOL

#include <array>
#include <cassert>
#include <condition_variable>
#include <cstddef>
#include <functional>
#include <mutex>
#include <optional>
#include <semaphore>
#include <shared_mutex>
#include <thread>
#include <vector>

namespace boxer {

template <typename Function, typename... Args>
class static_threadpool {
public:
	explicit static_threadpool(Function function)
	        : m_worker_semaphore(0), m_function(std::move(function)) {
		for (auto i = std::size_t{}; i < std::thread::hardware_concurrency(); ++i) {
			m_threads.emplace_back(&static_threadpool::thread_loop, this);
		}
	}

	~static_threadpool() {
		m_continue = false;
		release_workers();
	}

	void run(const Args&... arguments, std::size_t n, std::size_t chunk_size = 10u) {
		assert(chunk_size > 0);
		const auto runner_lock = std::unique_lock{m_runner_mutex};
		const auto num_threads = m_threads.size();
		m_arguments = std::tuple(std::cref(arguments)...);
		m_iterations = n;
		m_current_index = 0;
		m_chunk_size = chunk_size;
		release_workers();
		{
			auto completion_lock = std::unique_lock{m_completion_mutex};
			m_completion.wait(completion_lock,
			                  [&] { return m_completed_threads == num_threads; });
			m_completed_threads = 0u;
		}
	}

private:
	void thread_loop() {
		while (true) {
			m_worker_semaphore.acquire();
			if (not m_continue) {
				return;
			}
			for (auto i = m_current_index.fetch_add(m_chunk_size); i < m_iterations;
			     i = m_current_index.fetch_add(m_chunk_size)) {
				for (auto j = i; j < std::min(i + m_chunk_size, m_iterations);
				     ++j) {
					assert(m_arguments);
					std::apply(m_function,
					           std::tuple_cat(*m_arguments, std::tuple{j}));
				}
			}
			{
				// It should in theory be possible to replace this with an atomic
				// m_completed_threads, but for some reason that deadlocks:
				const auto completion_lock = std::unique_lock{m_completion_mutex};
				++m_completed_threads;
			}
			m_completion.notify_one();
		}
	}
	void release_workers() {
		m_worker_semaphore.release(static_cast<std::ptrdiff_t>(m_threads.size()));
	}
	std::counting_semaphore<> m_worker_semaphore;
	std::mutex m_runner_mutex;
	std::condition_variable m_completion;
	std::mutex m_completion_mutex;
	std::atomic_size_t m_current_index = 0u;
	std::vector<std::jthread> m_threads;
	std::size_t m_completed_threads = 0u;
	Function m_function;
	std::size_t m_iterations = 0u;
	std::size_t m_chunk_size = 1u;
	bool m_continue = true;
	std::optional<std::tuple<std::reference_wrapper<const Args>...>> m_arguments;
};

template <typename... Argument, typename Function>
static_threadpool<std::decay_t<Function>, Argument...> make_static_threadpool(Function f) {
	return static_threadpool<std::decay_t<Function>, Argument...>{std::forward<Function>(f)};
}

class threadpool {
public:
	explicit threadpool() : m_threadpool{runner{}} {}

	using Function = std::function<void(std::size_t)>;

	void run(Function function, std::size_t n, std::size_t chunk_size = 10u) {
		m_threadpool.run(function, n, chunk_size);
	}

private:
	struct runner {
		void operator()(Function f, std::size_t i) const { f(i); }
	};

	static_threadpool<runner, Function> m_threadpool;
};

inline threadpool& global_threadpool() {
	static auto tp = threadpool{};
	return tp;
}

} // namespace boxer

#endif // BOXER_THREADPOOL
