#ifndef BOXER_FORMAT_STYLES_HPP
#define BOXER_FORMAT_STYLES_HPP

#include <boxer/styles.hpp>

#include <format>

namespace boxer {

template <typename T>
struct styled {
	constexpr styled(const T& arg, style_update su={}, text_style ts = {}) : value{&arg}, su{su}, ts{ts} {}
	const T* value;
	style_update su = {};
	text_style ts = {};
};

template <typename T>
T&& add_style_context(T&& arg, text_style) {
	return std::forward<T&&>(arg);
}

template <typename T>
styled<T> add_style_context(styled<T> arg, text_style ts) {
	arg.ts = ts;
	return arg;
}

} // namespace boxer

template <typename T, typename Char>
struct std::formatter<boxer::styled<T>, Char> : std::formatter<T, Char> {
	template <class FormatContext>
	constexpr auto format(boxer::styled<T> t, FormatContext& fc) const {
		auto out = fc.out();
		out = std::format_to(out, "{}", boxer::to_escape_code(t.ts | t.su));
		fc.advance_to(out);
		out = std::formatter<T, Char>::format(*(t.value), fc);
		out = std::format_to(out, "{}", boxer::to_escape_code(t.ts));
		fc.advance_to(out);
		return out;
	}
};

#endif // BOXER_FORMAT_STYLES_HPP
