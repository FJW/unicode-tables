#ifndef UCT_GRAPHEMES_HPP
#define UCT_GRAPHEMES_HPP

#include <cassert>
#include <format>
#include <optional>
#include <ranges>
#include <string_view>
#include <tuple>
#include <utility>

#include <uct/width.hpp>

namespace uct {

struct grapheme {
	std::string_view str;
	std::size_t width;

	constexpr bool friend operator==(const uct::grapheme& lhs,
	                                 const uct::grapheme& rhs) = default;
};

namespace impl {
constexpr bool is_regional_indicator(char32_t c) { return 0x1F1E6 <= c and c <= 0x1F1FF; }

constexpr std::pair<grapheme, std::string_view>
peel_flag(char32_t first_half, std::string_view tail, std::string_view::const_iterator flag_start) {
	const auto [second_half, return_tail] = impl::peel_char(tail);
	if (not is_regional_indicator(second_half)) {
		throw std::runtime_error(std::format(
		        "Bad regional indicator (“flag-emoji”): Starts with {:04x} (“{}”), but "
		        "continues with {:04x}",
		        unsigned{first_half}, (first_half - 0x1f1e6) + 'A', unsigned{second_half}));
	}
	return {{{flag_start, return_tail.begin()}, 2}, return_tail};
}
} // namespace impl

inline constexpr bool is_newline(char32_t c) { return c == '\n'; }

constexpr std::pair<grapheme, std::string_view> peel_grapheme(std::string_view s,
                                                              std::size_t emoji_width = 2) {
	assert(not s.empty());
	if (const auto [emoji, tail] = impl::peel_emoji(s); not emoji.empty()) {
		return {{emoji, emoji_width}, tail};
	}
	auto consume_next = true;
	auto width = std::size_t{};
	auto consumed = std::size_t{};
	auto start = s.begin();
	while (not s.empty()) {
		const auto [c, tail] = impl::peel_char(s);
		if (impl::is_regional_indicator(c)) {
			if (consumed) {
				return {{{start, s.begin()}, width}, s};
			} else {
				return impl::peel_flag(c, tail, start);
			}
		}
		const auto w = char_width(c);
		const auto is_nl = is_newline(c);
		if (consumed == 0 and is_nl) {
			return {{{start, tail.begin()}, width}, tail};
		}
		if ((w and not consume_next) or is_nl) {
			return {{{start, s.begin()}, width}, s};
		}
		if (w) {
			consume_next = false;
			// TODO: deal with joiners.
		}
		s = tail;
		width += w;
		++consumed;
	}
	const auto ret = std::string_view(start, s.begin());
	return std::pair{grapheme{ret, width}, s};
}

class grapheme_iterator_end_sentinel {
public:
	constexpr grapheme_iterator_end_sentinel() {}
};

class grapheme_iterator {
public:
	using difference_type = std::ptrdiff_t;
	using value_type = grapheme;

	constexpr explicit grapheme_iterator() {}

	constexpr explicit grapheme_iterator(std::string_view s) {
		if (s.empty()) {
			m_grapheme = std::nullopt;
			m_tail = s;
		} else {
			std::tie(m_grapheme, m_tail) = peel_grapheme(s);
		}
	}

	constexpr const grapheme& operator*() const {
		assert(m_grapheme);
		return *m_grapheme;
	}

	constexpr const grapheme* operator->() const {
		assert(m_grapheme);
		return &(*m_grapheme);
	}

	constexpr grapheme_iterator& operator++() {
		assert(m_grapheme);
		if (m_tail.empty()) {
			m_grapheme = std::nullopt;
		} else {
			std::tie(m_grapheme, m_tail) = peel_grapheme(m_tail);
		}
		return *this;
	}
	constexpr grapheme_iterator operator++(int) {
		auto ret = *this;
		++(*this);
		return ret;
	}

	constexpr friend bool operator==(grapheme_iterator lhs, grapheme_iterator rhs) {
		if (not lhs.m_grapheme and not rhs.m_grapheme) {
			return true;
		} else if ((lhs.m_grapheme and not rhs.m_grapheme) or
		           (not lhs.m_grapheme and rhs.m_grapheme)) {
			return false;
		} else {
			return std::tuple(lhs.m_grapheme->str.begin(), lhs.m_grapheme->str.end(),
			                  lhs.m_tail.end()) ==
			       std::tuple(rhs.m_grapheme->str.begin(), rhs.m_grapheme->str.end(),
			                  rhs.m_tail.end());
		}
	}
	constexpr friend bool operator==(grapheme_iterator it, grapheme_iterator_end_sentinel) {
		return it.m_grapheme == std::nullopt;
	}
	constexpr friend bool operator==(grapheme_iterator_end_sentinel end, grapheme_iterator it) {
		return it == end;
	}

	std::string_view::const_iterator underlying_begin() const {
		if (m_grapheme) {
			return m_grapheme->str.cbegin();
		} else {
			return m_tail.cbegin();
		}
	}
	std::string_view::const_iterator underlying_end() const {
		if (m_grapheme) {
			return m_grapheme->str.cend();
		} else {
			return m_tail.cbegin();
		}
	}

private:
	std::optional<grapheme> m_grapheme;
	std::string_view m_tail;
};

class grapheme_range {
public:
	constexpr grapheme_range(std::string data) : m_data{std::move(data)} {}

	constexpr grapheme_iterator begin() const { return grapheme_iterator{m_data}; }
	constexpr grapheme_iterator_end_sentinel end() const {
		return grapheme_iterator_end_sentinel{};
	}

	constexpr grapheme_iterator cbegin() const { return grapheme_iterator{m_data}; }
	constexpr grapheme_iterator_end_sentinel cend() const {
		return grapheme_iterator_end_sentinel{};
	}

	constexpr bool empty() const { return m_data.empty(); }

private:
	std::string m_data;
};

class grapheme_view : public std::ranges::view_interface<grapheme_view> {
public:
	constexpr grapheme_view(std::string_view data) : m_data{data} {}

	constexpr grapheme_iterator begin() const { return grapheme_iterator{m_data}; }
	constexpr grapheme_iterator_end_sentinel end() const {
		return grapheme_iterator_end_sentinel{};
	}

	constexpr grapheme_iterator cbegin() const { return grapheme_iterator{m_data}; }
	constexpr grapheme_iterator_end_sentinel cend() const {
		return grapheme_iterator_end_sentinel{};
	}

	constexpr bool empty() const { return m_data.empty(); }

private:
	std::string_view m_data;
};

} // namespace uct

#endif // UCT_GRAPHEMES_HPP
