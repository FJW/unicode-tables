#ifndef UCT_SPLIT_HPP
#define UCT_SPLIT_HPP

#include <functional>
#include <stdexcept>
#include <string_view>
#include <tuple>
#include <vector>

#include <uct/graphemes.hpp>

namespace uct {

namespace impl {
inline constexpr bool is_newline(std::string_view s) { return s == "\n"; }
} // namespace impl

inline constexpr std::vector<std::string_view> split_lines(std::string_view s,
                                                           std::size_t max_width) {
	auto ret = std::vector<std::string_view>{};
	auto start = s.begin();
	auto current_width = std::size_t{};
	while (not s.empty()) {
		auto [grapheme, tail] = peel_grapheme(s);
		s = tail;
		if (impl::is_newline(grapheme.str)) {
			ret.emplace_back(start, grapheme.str.begin());
			if (not s.empty()) {
				std::tie(grapheme, tail) = peel_grapheme(s);
				start = grapheme.str.begin();
				current_width = grapheme.width;
			} else {
				return ret;
			}
		} else if (current_width + grapheme.width > max_width) {
			ret.emplace_back(start, grapheme.str.begin());
			start = grapheme.str.begin();
			current_width = grapheme.width;
		} else {
			current_width += grapheme.width;
		}
	}
	if (start != s.end()) {
		ret.emplace_back(start, s.end());
	}
	return ret;
}

namespace impl {
constexpr inline std::tuple<grapheme_iterator, grapheme_iterator, std::size_t>
skip_while(grapheme_iterator it, auto end, auto pred) {
	auto skipped_width = std::size_t{};
	auto prev_it = it;
	while (it != end and pred(*it)) {
		skipped_width += it->width;
		prev_it = it++;
	}
	return {prev_it, it, skipped_width};
}

enum class character_newline_rule {
	avoid,
	optional,
	forced,
	add_hyphen,
	drop_char,

};

constexpr std::tuple<character_newline_rule, std::size_t> grapheme_newline_rule(grapheme g) {
	if (g.str == " ") {
		return {character_newline_rule::drop_char, 0};
	} else if (g.str == "\n") {
		return {character_newline_rule::forced, 0};
	} else if (g.str == "-") {
		return {character_newline_rule::optional, 1};
	} else {
		return {character_newline_rule::avoid, g.width+1};
	}
}

struct nom_result {
	grapheme_iterator nom_end;
	grapheme_iterator next_start;
	std::size_t length;
	bool add_hyphen = false;
};

inline constexpr auto consume_line(grapheme_iterator it, std::size_t n) -> nom_result {
	const auto end = grapheme_iterator_end_sentinel{};
	auto break_points = std::array<std::optional<nom_result>, 3> {};
	auto line_length = std::size_t{};
	auto undropped_length = std::size_t{};
	while (it !=  end ) {
		const auto next = std::next(it);
		const auto width = it->width;
		const auto [newline_rule, newline_width] = grapheme_newline_rule(*it);
		const auto line_ended_length = static_cast<std::size_t>(line_length + newline_width);
		line_length += width;
		if (line_ended_length > n and line_length > n and undropped_length > n) {
			break;
		}
		switch(newline_rule) {
			case character_newline_rule::forced:
				// TODO: this might be too long…
				return {it, next, line_ended_length};
			case character_newline_rule::drop_char:
				if (break_points[0] and break_points[0]->next_start == it) {
					break_points[0]->next_start = next;
				} else {
					undropped_length = line_ended_length;
					break_points[0] = {it, next, line_ended_length};
				}
				break;
			case character_newline_rule::optional:
				undropped_length = line_length;
				if (line_ended_length <= n) {
					break_points[0] = {next, next, line_ended_length};
				}
				break;
			case character_newline_rule::add_hyphen:
				undropped_length = line_length;
				if (line_ended_length <= n) {
					break_points[1] = {next, next, line_ended_length, true};
				}
				break;
			case character_newline_rule::avoid:
				undropped_length = line_length;
				if (line_ended_length <= n) {
					break_points[2] = {next, next, line_ended_length, true};
				}
				break;
		}
		it = next;

	}
	if (it == end and line_length <= n) {
		return {it, it, line_length};
	}
	const auto best_result = std::find_if(break_points.begin(), break_points.end(), [](const auto& r)->bool{return r != std::nullopt;});
	if (best_result == break_points.end()) {
		throw std::runtime_error{"no possible linebreak found"};
	}
	return **best_result;
}
} // namespace impl

struct wrapped_line {
	std::string_view line;
	bool add_hyphen = false;
	std::size_t width;
};


inline constexpr std::vector<wrapped_line>
split_words(std::string_view s, std::size_t max_width) {
	if (s.empty()) {
		return {{s, false, 0}};
	}
	auto it = grapheme_iterator{s};
	const auto end = grapheme_iterator_end_sentinel{};
	auto ret = std::vector<wrapped_line>{};
	while(it != end) {
		const auto [line_end, line_start, width, add_hyphen] = impl::consume_line(it, max_width);
		const auto line = std::string_view{it.underlying_begin(), line_end.underlying_begin()};
		ret.emplace_back(line, add_hyphen, width);
		it = line_start;
	}
	return ret;
}

} // namespace uct

#endif // UCT_SPLIT_HPP

