#include <boxer/table.hpp>

#include <limits>
#include <stdexcept>
#include <format>

#include <uct/width.hpp>

namespace boxer {

namespace {
std::vector<std::size_t> col_sizes(const std::vector<std::vector<std::string>>& data) {
	auto columns = std::vector<std::size_t>{};
	for (const auto& row : data) {
		if (row.size() > columns.size()) {
			columns.resize(row.size());
		}
		for (auto i = std::size_t{}; i < row.size(); ++i) {
			columns[i] = std::max(columns[i], uct::string_width(row[i]));
		}
	}
	return columns;
}
} // anonymous namespace

std::string to_table(const std::vector<std::vector<std::string>>& data,
                     const std::function<std::string(int, std::size_t)> seps) {
	const auto columns = col_sizes(data);
	if (columns.size() > std::numeric_limits<int>::max()) {
		throw std::overflow_error{"Too many columns"};
	}
	auto ret = std::string{};
	for (auto i = std::size_t{}; i < data.size(); ++i) {
		const auto& row = data[i];
		ret += seps(-1, i);
		for (auto j = std::size_t{}; j < columns.size(); ++j) {
			auto field = j < row.size() ? row[j] : std::string{};
			ret += field + std::string(columns[j] - uct::string_width(field), ' ');
			if (j + 1u < columns.size()) {
				ret += seps(static_cast<int>(j), i);
			}
		}
		ret += seps(-2, i);
		ret += '\n';
	}
	return ret;
}
std::string to_table(const std::vector<std::vector<std::string>>& data, const std::string_view sep,
                     const std::string_view outer) {
	return to_table(data, [&](std::size_t, int j) { return std::string{j < 0 ? outer : sep}; });
}

std::string to_matrix(const std::vector<std::vector<std::string>>& data,
                      const matrix_style& style) {
	return to_table(data, [&](int col, std::size_t row) -> std::string {
		if (col >= 0) {
			return style.seperator;
		} else if (col == -1) {
			if (row == 0) {
				return style.left_top;
			} else if (row == data.size() - 1u) {
				return style.left_bottom;
			} else {
				return style.left;
			}
		} else if (col == -2) {
			if (row == 0) {
				return style.right_top;
			} else if (row == data.size() - 1u) {
				return style.right_bottom;
			} else {
				return style.right;
			}
		}
		throw std::logic_error{std::format("Invalid coordinate: {}, {}", col, row)};
	});
}
} // namespace boxer
