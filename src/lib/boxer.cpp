#include <boxer/boxer.hpp>
#include <span>

#include <ranges>
#include <uct/split.hpp>
#include <uct/width.hpp>

#include <boxer/terminal_dimensions.hpp>

namespace boxer {

namespace {
std::string line(std::size_t n, const style& style) {
	auto ret = std::string{};
	ret.reserve(style.bs.v.size() * n);
	for (auto i = 0u; i < n; ++i) {
		ret.append(style.bs.h);
	}
	return ret;
}

std::string box_left(const std::span<const style>& boxes) {
	auto ret = std::string{};
	for (auto&& box : boxes) {
		ret += to_escape_code(box.ts);
		ret += box.bs.v;
	}
	return ret;
}
std::string box_right(const std::span<const style>& boxes) {
	auto ret = std::string{};
	// This doesn’ work with Clang, even though it should:
	// for (auto&& box : boxes | std::views::reverse) {
	// So instead do this:
	for (auto it = boxes.rbegin(); it != boxes.rend(); ++it) {
		ret += to_escape_code(it->ts);
		ret += it->bs.v;
	}
	ret += reset_code();
	return ret;
}

void enforce(bool b, std::string_view msg) {
	if (not b) {
		throw boxer_error{std::string{msg}};
	}
}
} // anonymous namespace

boxer::boxer(std::ostream& out) : m_out{&out} {}

void boxer::start_box(std::string_view label, const box_style& bs, style_update su,
                      style_update header_style) {
	const auto lock = std::unique_lock{m_mutex};
	auto ts = text_style{};
	if (not m_active_boxes.empty()) {
		ts = m_active_boxes.back().ts;
	}
	ts |= su;
	const auto w = get_terminal_dimensions().width;
	const auto n = m_active_boxes.size();
	enforce(w > 2 * n, "boxer-error: Not enough space to draw box.");
	auto header = box_left(m_active_boxes);
	header += to_escape_code(ts);
	if (label.empty()) {
		header += bs.tl;
		header += line(w - 2 * n - 2, {bs, ts});
		header += bs.tr;
	} else {
		const auto label_width = uct::string_width(label);
		enforce(w > 2 * n + label_width + 3, "boxer-error: Not enough space to draw box.");
		header += bs.tl;
		header += bs.h;
		header += bs.r;
		header += to_escape_code(ts | header_style);
		header += label;
		header += to_escape_code(ts);
		header += bs.l;
		header += line(w - 2 * n - label_width - 5, {bs, ts});
		header += bs.tr;
	}
	header += reset_code();
	header += box_right(m_active_boxes);
	header += '\n';
	m_active_boxes.push_back({bs, ts});
	*m_out << header;
}

void boxer::end_box() {
	const auto lock = std::unique_lock{m_mutex};
	enforce(not m_active_boxes.empty(), "boxer-error: no box to close");
	const auto w = get_terminal_dimensions().width;
	auto style = m_active_boxes.back();
	m_active_boxes.pop_back();
	const auto n = m_active_boxes.size();
	enforce(w > 2 * n + 2, "boxer-error: Not enough space to end box.");
	auto footer = box_left(m_active_boxes);
	footer += to_escape_code(style.ts);
	footer += style.bs.bl;
	footer += line(w - 2 * n - 2, style);
	footer += style.bs.br;
	footer += reset_code();
	footer += box_right(m_active_boxes);
	footer += '\n';
	*m_out << footer;
}

void boxer::end_all_boxes() {
	const auto lock = std::unique_lock{m_mutex};
	const auto n = m_active_boxes.size();
	for (auto i = std::size_t{}; i < n; ++i) {
		end_box();
	}
}

void boxer::print_fitting_line(std::string_view s, std::size_t w) {
	const auto lock = std::unique_lock{m_mutex};
	const auto n = m_active_boxes.size();
	const auto sw = uct::string_width(s);
	assert(w >= 2 * n + sw && "boxer-error: Not enough space to draw line.");
	auto line = box_left(m_active_boxes);
	line += s;
	line += std::string(w - 2 * n - sw, ' ');
	line += box_right(m_active_boxes);
	line += '\n';
	*m_out << line;
}

void boxer::print_fitting_line(text_style ts, std::string_view s, std::size_t w) {
	const auto lock = std::unique_lock{m_mutex};
	const auto n = m_active_boxes.size();
	const auto sw = uct::string_width(s);
	if (w < 2 * n + sw) {
		std::cerr << "Boxer-Error: Not enough space for line:\n"
		          << std::format("s=“{}”\nw={}\n", s, w);
	}
	assert(w >= 2 * n + sw && "boxer-error: Not enough space to draw line.");
	auto line = box_left(m_active_boxes);
	line += to_escape_code(ts);
	line += s;
	line += std::string(w - 2 * n - sw, ' ');
	line += reset_code();
	line += box_right(m_active_boxes);
	line += '\n';
	*m_out << line;
}

void boxer::print_string(std::string_view s) {
	const auto lock = std::unique_lock{m_mutex};
	const auto w = get_terminal_dimensions().width;
	const auto lines = uct::split_words(s, w - 2 * m_active_boxes.size());
	for (auto&& [l, hypen, actual_width] : lines) {
		if (hypen) {
			const auto str = std::string{l} + std::string{"-"};
			print_fitting_line(str, w);
		} else {
			print_fitting_line(l, w);
		}
	}
}

void boxer::print_string(style_update su, std::string_view s) {
	const auto lock = std::unique_lock{m_mutex};
	const auto w = get_terminal_dimensions().width;
	const auto lines = uct::split_words(s, w - 2 * m_active_boxes.size());
	auto ts = (m_active_boxes.empty() ? text_style{} : m_active_boxes.back().ts) | su;
	for (auto&& [l, hypen, actual_width] : lines) {
		if (hypen) {
			const auto str = std::string{l} + std::string{"-"};
			print_fitting_line(ts, str, w);
		} else {
			print_fitting_line(ts, l, w);
		}
	}
}

void boxer::unchecked_print_single_line(std::string_view s) {
	auto line = std::string{};
	line += box_left(m_active_boxes);
	line += s;
	line += box_right(m_active_boxes);
	line += "\n";
	const auto lock = std::unique_lock{m_mutex};
	*m_out << line;
}
void boxer::unchecked_print(std::string_view s) {
	const auto lock = std::unique_lock{m_mutex};
	*m_out << s;
}

void boxer::rule() {
	const auto lock = std::unique_lock{m_mutex};
	const auto w = get_terminal_dimensions().width;
	const auto n = m_active_boxes.size();
	enforce(w > 2 * n, "boxer-error: Not enough space to draw rule.");
	auto boxes = std::span{m_active_boxes};
	auto line = box_left(boxes.subspan(0, n - 1));
	const auto& style = m_active_boxes.back();
	line += to_escape_code(style.ts);
	line += style.bs.l;
	line += ::boxer::line(w - 2 * n, style);
	line += style.bs.r;
	line += reset_code();
	line += box_right(boxes.subspan(0, n - 1));
	line += '\n';
	*m_out << line;
}

std::size_t boxer::text_width() const {
	const auto w = get_terminal_dimensions().width;
	const auto n = m_active_boxes.size();
	enforce(w > 2 * n, "boxer-error: Not enough space to draw box.");
	return w - 2 * n;
}

} // namespace boxer
