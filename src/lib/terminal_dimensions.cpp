#include <boxer/terminal_dimensions.hpp>

#if defined(__unix__) || defined(__unix) || defined(__APPLE__)

#include <sys/ioctl.h>

namespace boxer {
terminal_dimensions get_terminal_dimensions() {
	auto w = winsize{};
	if (ioctl(0, TIOCGWINSZ, &w) == -1) {
		return {80, 60};
	}
	return {w.ws_col, w.ws_row};
}
} // namespace boxer

#else

namespace boxer {
terminal_dimensions get_terminal_dimensions() { return {80, 60}; }
} // namespace boxer

#endif
