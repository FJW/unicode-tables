#include <clp/clp.hpp>

#include <istream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("basic types", "[basic]") {
	const auto parser = clp::arg_parser{clp::required_argument<"i,int",int>{},
	                                    clp::required_argument<"s,str",std::string>{},
	                                    clp::required_argument<"f,float:𝑓",float>{}};
	const auto args = std::vector<std::string>{"-i3", "-s", "foo bar", "--float", "1.0"};
	const auto parsed = parser.parse(args);
	const auto [i, s, f] = parsed;
	CHECK(i == 3);
	CHECK(s == "foo bar");
	CHECK(f == 1.0);
	CHECK(parsed.get<"int">() == 3);
	CHECK(parsed.get<"𝑓">() == 1.0);
}

struct customn_type {
	int i = 23;
};

std::istream& operator>>(std::istream& s, customn_type& x) { return s >> x.i; }

TEST_CASE("customn type", "[basic]") {
	auto parser = clp::arg_parser{
	        clp::required_argument<"x,xxx",customn_type>{},
	};
	const auto args = std::vector<std::string>{"-x3"};
	const auto [x] = parser.parse(args);
	CHECK(x.i == 3);
}

TEST_CASE("optional arguments", "[basic]") {
	auto parser = clp::arg_parser{clp::optional_argument<"i,int", int>{},
	                              clp::optional_argument<"s,str", std::string>{}};
	const auto args = std::vector<std::string>{"-i3"};
	const auto [i, s] = parser.parse(args);
	CHECK(i == std::optional{3});
	CHECK(s == std::nullopt);
}

TEST_CASE("defaulted arguments", "[basic]") {
	auto parser = clp::arg_parser{clp::defaulted_argument<"i,int", int>{23},
	                              clp::defaulted_argument<"s,str", std::string>{"foo"}};
	const auto args = std::vector<std::string>{"-sbar"};
	const auto [i, s] = parser.parse(args);
	CHECK(i == 23);
	CHECK(s == "bar");
}

TEST_CASE("boolean flags", "[basic]") {
	auto parser = clp::arg_parser{
	        clp::bool_flag<"b,bool">{},
	        clp::bool_flag<"c,other-bool">{},
	};
	const auto args = std::vector<std::string>{"-b"};
	const auto [b0, b1] = parser.parse(args);
	CHECK(b0);
	CHECK(not b1);
}

TEST_CASE("boolean counters", "[basic]") {
	auto parser = clp::arg_parser{
	        clp::bool_counter<"b,bool">{},
	        clp::bool_counter<"c,other-bool">{},
	};
	const auto args = std::vector<std::string>{"-b", "-c", "--bool", "--other-bool", "-b"};
	const auto [b0, b1] = parser.parse(args);
	CHECK(b0 == 3);
	CHECK(b1 == 2);
}

TEST_CASE("collected arguments", "[basic]") {
	auto parser = clp::arg_parser{clp::collected_argument<"i,int",int>{},
	                              clp::collected_argument<"s,str",std::string>{}};
	const auto args = std::vector<std::string>{"-i1", "--str", "foo", "-sbar", "--int", "2",
	                                           "-i3", "-i",    "4",   "-s",    "baz"};
	const auto [i, s] = parser.parse(args);
	const auto i_expected = std::vector{1, 2, 3, 4};
	const auto s_expected = std::vector<std::string>{"foo", "bar", "baz"};
	CHECK(i == i_expected);
	CHECK(s == s_expected);
}

TEST_CASE("long options with =", "[basic]") {
	auto parser = clp::arg_parser{clp::required_argument<"str0",std::string>{},
	                              clp::required_argument<"str1",std::string>{}};
	const auto args = std::vector<std::string>{"--str0=foo", "--str1", "bar"};
	const auto [s0, s1] = parser.parse(args);
	CHECK(s0 == "foo");
	CHECK(s1 == "bar");
}
