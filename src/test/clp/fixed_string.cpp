#include <clp/fixed_string.hpp>

using namespace clp::impl;

static_assert(split<fixed_string_literal<"Foo,Bar">, ','>() ==
              string_list<fixed_string_literal<"Foo">, fixed_string_literal<"Bar">>{});
static_assert(string_list<fixed_string_literal<"Foo">, fixed_string_literal<"Bar">>{}.get<0>() ==
              fixed_string_literal<"Foo">);
static_assert(not contains_non_empty_duplicates(string_list<"a","b">{}));
static_assert(contains_non_empty_duplicates(string_list<"a","b","a">{}));
static_assert(not contains_non_empty_duplicates(string_list<"","b">{}));
static_assert(not contains_non_empty_duplicates(string_list<"","">{}));

static_assert(get_value<"Foo">(string_list<"Foo", "Bar">{}, std::tuple{23, "42"}) == 23);
static_assert(extract_short_name<"i,int">() == fixed_string_literal<"i">);
static_assert(extract_long_name<"int">() == fixed_string_literal<"int">);
static_assert(extract_long_name<"i,int">() == fixed_string_literal<"int">);
static_assert(extract_long_name<"int:foo">() == fixed_string_literal<"int">);
static_assert(extract_long_name<"i,int:foo">() == fixed_string_literal<"int">);
static_assert(extract_long_name<"i,:foo">() == fixed_string_literal<"">);
static_assert(extract_identifier_name<"i,:foo">() == fixed_string_literal<"foo">);
static_assert(extract_identifier_name<"i,foo">() == fixed_string_literal<"foo">);
