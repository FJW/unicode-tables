#include <clp/clp.hpp>

#include <optional>
#include <string>
#include <vector>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("fullfilled dependencies", "[dependencies]") {
	{
		auto parser = clp::arg_parser{
		        clp::optional_argument<"x,xxx", int>{},
		        clp::optional_argument<"y,yyy", int>{clp::dependencies{{"xxx"}}}};
		const auto args = std::vector<std::string>{"-x3", "--yyy", "4"};
		const auto [x, y] = parser.parse(args);
		CHECK(x == 3);
		CHECK(y == 4);
	}
	{
		auto parser = clp::arg_parser{
		        clp::optional_argument<"x,xxx", int>{clp::dependencies{{"yyy"}}},
		        clp::optional_argument<"y,yyy", int>{clp::dependencies{{"xxx"}}}};
		const auto args = std::vector<std::string>{"-x3", "-y4"};
		const auto [x, y] = parser.parse(args);
		CHECK(x == 3);
		CHECK(y == 4);
	}
}

TEST_CASE("unfullfilled dependencies", "[dependencies]") {
	auto parser = clp::arg_parser{
	        clp::optional_argument<"x,xxx", int>{},
	        clp::optional_argument<"y,yyy", int>{clp::dependencies{{"xxx"}}}};
	const auto args = std::vector<std::string>{"-y3"};
	CHECK_THROWS_AS(parser.parse(args), clp::bad_argument);
}

TEST_CASE("irrelevant dependencies", "[dependencies]") {
	auto parser = clp::arg_parser{
	        clp::optional_argument<"x,xxx", int>{},
	        clp::optional_argument<"y,yyy", int>{clp::dependencies{{"xxx"}}}};
	const auto args = std::vector<std::string>{"-x3"};
	const auto [x, y] = parser.parse(args);
	CHECK(x == 3);
	CHECK(y == std::nullopt);
}

TEST_CASE("nonviolated conflicts", "[dependencies]") {
	{
		auto parser = clp::arg_parser{
		        clp::optional_argument<"x,xxx", int>{},
		        clp::optional_argument<"y,yyy", int>{clp::conflicts{{"xxx"}}}};
		const auto args = std::vector<std::string>{"--yyy", "4"};
		const auto [x, y] = parser.parse(args);
		CHECK(x == std::nullopt);
		CHECK(y == 4);
	}
	{
		auto parser = clp::arg_parser{
		        clp::optional_argument<"x,xxx", int>{clp::conflicts{{"yyy"}}},
		        clp::optional_argument<"y,yyy", int>{clp::conflicts{{"xxx"}}}};
		const auto args = std::vector<std::string>{"-x3"};
		const auto [x, y] = parser.parse(args);
		CHECK(x == 3);
		CHECK(y == std::nullopt);
	}
}

TEST_CASE("violated conflicts", "[dependencies]") {
	auto parser = clp::arg_parser{
	        clp::optional_argument<"x,xxx", int>{},
	        clp::optional_argument<"y,yyy", int>{clp::conflicts{{"xxx"}}}};
	const auto args = std::vector<std::string>{"-y3", "-x4"};
	CHECK_THROWS_AS(parser.parse(args), clp::bad_argument);
}

