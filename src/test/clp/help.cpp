#include <clp/clp.hpp>

#include <istream>
#include <optional>
#include <sstream>
#include <string>
#include <vector>

#include <catch2/catch_test_macros.hpp>

#include <iostream>

namespace {
struct customn_type {};

inline std::istream& operator>>(std::istream& s, customn_type&) { return s; }
} // anonymous namespace

std::string replace_all(std::string inout, std::string_view what, std::string_view with) {
	for (std::string::size_type pos{};
	     inout.npos != (pos = inout.find(what.data(), pos, what.length()));
	     pos += with.length()) {
		inout.replace(pos, what.length(), with.data(), with.length());
	}
	return inout;
}

TEST_CASE("help-generation", "[help]") {
	const auto parser = clp::arg_parser{
	        clp::required_argument<"i,int", int>{},
	        clp::optional_argument<"s,str", std::string>{clp::description{{"some string"}}},
	        clp::optional_argument<"b,bool", bool>{clp::description{{"some bool"}}},
	        clp::defaulted_argument<"f,float", float>{4.2f, clp::description{{"some float"}}},
	        clp::bool_flag<"F,flag">{},
	        clp::bool_counter<"c,ctr">{},
	        clp::collected_argument<"C,coll", float>{},
	        clp::defaulted_argument<"cust", customn_type>{customn_type{}}};
	const auto expected = "Usage: test [options]\n"
	                      "  -i, --int   : ⟨integer⟩        \n"
	                      "  -s, --str   : [string]         some string\n"
	                      "  -b, --bool  : [bool]           some bool\n"
	                      "  -f, --float : [float]    = 4.2 some float\n"
	                      "  -F, --flag  : [flag]     = 0   \n"
	                      "  -c, --ctr   : [flag...]  = 0   \n"
	                      "  -C, --coll  : [float...]       \n"
	                      "      --cust  : [?]              \n";
	auto s = std::ostringstream{};
	parser.print_help("test", s);
	CHECK(s.str() == expected);
}
