#include <clp/clp.hpp>

#include <string>
#include <vector>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("pure positional argument", "[positional]") {
	const auto parser = clp::arg_parser{clp::required_positional_argument<"int", int>{}};
	const auto args = std::vector<std::string>{"2342"};
	const auto [i] = parser.parse(args);
	CHECK(i == 2342);
}

TEST_CASE("mixed positional argument", "[positional]") {
	const auto parser = clp::arg_parser{clp::bool_flag<"b,bool">{},
	                                    clp::required_positional_argument<"int", int>{},
	                                    clp::bool_counter<"c,ctr">{}};
	const auto args = std::vector<std::string>{"-c", "-b", "2342", "-c"};
	const auto [b, i, c] = parser.parse(args);
	CHECK(b);
	CHECK(i == 2342);
	CHECK(c == 2);
}

TEST_CASE("multiple positional argument", "[positional]") {
	const auto parser = clp::arg_parser{
	        clp::bool_flag<"b,bool">{}, clp::required_positional_argument<"int", int>{},
	        clp::required_positional_argument<"float", float>{}, clp::bool_counter<"c,ctr">{}};
	const auto args = std::vector<std::string>{"-c", "-b", "2342", "-c", "0.5"};
	const auto [b, i, f, c] = parser.parse(args);
	CHECK(b);
	CHECK(i == 2342);
	CHECK(f == 0.5);
	CHECK(c == 2);
}

TEST_CASE("missing positional argument", "[positional]") {
	const auto parser = clp::arg_parser{
	        clp::bool_flag<"b,bool">{}, clp::required_positional_argument<"int", int>{},
	        clp::required_positional_argument<"float", float>{}, clp::bool_counter<"c,ctr">{}};
	const auto args = std::vector<std::string>{"-c", "-b", "2342", "-c"};
	CHECK_THROWS_AS(parser.parse(args), clp::bad_argument);
}
