#include <boxer/threadpool.hpp>
#include <thread>

#define CATCH_CONFIG_ENABLE_BENCHMARKING
#include <catch2/catch_test_macros.hpp>
#include <catch2/benchmark/catch_benchmark.hpp>

TEST_CASE("Threadpool", "") {
	auto threadpool = boxer::threadpool{};
	auto vec = std::vector<std::size_t>(10, 0);
	threadpool.run([&](auto i) { vec[i] += i; }, vec.size());
	CHECK(vec == std::vector<std::size_t>{0, 1, 2, 3, 4, 5, 6, 7, 8, 9});
	threadpool.run([&](auto i) { vec[i] *= i; }, vec.size());
	CHECK(vec == std::vector<std::size_t>{0, 1, 4, 9, 16, 25, 36, 49, 64, 81});
}

unsigned computational_delay(unsigned n, unsigned load, unsigned load_factor) {
	auto ret = 0u;
	for (auto load_i = 0u; load_i < load_factor; ++load_i) {
		for (auto i = 0u; i < load; ++i) {
			for (auto j = i; j < load; ++j) {
				for (auto k = j; k < load; ++k) {
					ret += (i * j * k) % (n + 2);
				}
			}
		}
	}
	return ret;
}

TEST_CASE("Threadpool Benchmark", "[!benchmark]") {
	const auto n = std::thread::hardware_concurrency() * 10u;
	auto vec = std::vector<std::size_t>(n, 0);

	const auto make_run = [&](unsigned chunk_size, unsigned load, unsigned load_factor) {
		if (chunk_size) {
			boxer::global_threadpool().run(
			        [&](auto i) {
				        vec[i] = computational_delay(static_cast<unsigned>(i), load,
				                                     load_factor);
			        },
			        vec.size(), chunk_size);
			return &vec;
		} else {
			for (auto i = std::size_t{}; i < vec.size(); ++i) {
				vec[i] = computational_delay(static_cast<unsigned>(i), load,
				                             load_factor);
			}
			return &vec;
		}
	};
	for (auto load : {1u, 10u, 50u}) {
		for (auto load_factor : {1u, 2u, 3u}) {
			for (auto chunk_size : {0u, 1u, 2u, 5u, 10u, 20u, n}) {
				BENCHMARK(std::format("load={}, lfac={}, cs={}", load, load_factor,
				                      chunk_size ? std::to_string(chunk_size)
				                                 : "control")) {
					return make_run(chunk_size, load, load_factor);
				};
			}
		}
	}
}

TEST_CASE("Static Threadpool Benchmark", "[!benchmark]") {
	const auto n = std::thread::hardware_concurrency() * 10u;
	auto vec = std::vector<std::size_t>(n, 0);

	auto threadpool = boxer::make_static_threadpool<unsigned, unsigned>(
	        [&](unsigned load, unsigned load_factor, std::size_t i) {
		        vec[i] = computational_delay(static_cast<unsigned>(i), load, load_factor);
	        });

	for (auto load : {1u, 10u, 50u}) {
		for (auto load_factor : {1u, 2u, 3u}) {
			for (auto chunk_size : {1u, 2u, 5u, 10u, 20u, n}) {
				BENCHMARK(std::format("load={}, lfac={}, cs={}", load, load_factor,
				                      chunk_size ? std::to_string(chunk_size)
				                                 : "control")) {
					return threadpool.run(load, load_factor, n, chunk_size);
				};
			}
		}
	}
}
