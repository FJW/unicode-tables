#include <uct/split.hpp>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Split Words", "[uct]") {
	const auto long_text = std::string{"Trans Rights are Human              Rights"};
	{
		const auto lines = uct::split_words(long_text, 12);
		REQUIRE(lines.size() == 3);
		{
			const auto [line, hyphen, width] = lines.at(0);
			CHECK(line == "Trans Rights");
			CHECK(width == 12);
			CHECK(not hyphen);
		}
		{
			const auto [line, hyphen, width] = lines.at(1);
			CHECK(line == "are Human");
			CHECK(width == 9);
			CHECK(not hyphen);
		}
		{
			const auto [line, hyphen, width] = lines.at(2);
			CHECK(line == "Rights");
			CHECK(width == 6);
			CHECK(not hyphen);
		}
	}
	{
		const auto lines = uct::split_words(long_text, 13);
		REQUIRE(lines.size() == 3);
		const auto [line, hyphen, width] = lines.at(0);
		CHECK(line == "Trans Rights");
		CHECK(width == 12);
		CHECK(not hyphen);
	}
	{
		const auto text = std::string{"foo-bar"};
		const auto lines = uct::split_words(text, 4);
		REQUIRE(lines.size() == 2);
		const auto [line, hyphen, width] = lines.at(0);
		CHECK(line == "foo-");
		CHECK(width == 4);
		CHECK(not hyphen);
	}
	{
		const auto text = std::string{"foo-bar"};
		const auto lines = uct::split_words(text, 3);
		REQUIRE(lines.size() == 3);
		const auto [line, hyphen, width] = lines.at(0);
		CHECK(line == "fo");
		CHECK(width == 3);
		CHECK(hyphen);
	}
}
