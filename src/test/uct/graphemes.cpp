#include <uct/graphemes.hpp>

#include <catch2/catch_test_macros.hpp>

#include <string_view>
#include <ranges>

using namespace std::literals;

static_assert(uct::peel_grapheme("ab") == std::pair{uct::grapheme{"a", 1}, "b"sv});
static_assert(uct::peel_grapheme("a") == std::pair{uct::grapheme{"a", 1}, ""sv});
static_assert(uct::peel_grapheme("\n") == std::pair{uct::grapheme{"\n", 0}, ""sv});
static_assert(uct::peel_grapheme("⚧\n") == std::pair{uct::grapheme{"⚧", 1}, "\n"sv});
static_assert(uct::peel_grapheme("🏳️‍⚧️X") ==
              std::pair{uct::grapheme{"🏳️‍⚧️", 2}, "X"sv});
static_assert(uct::peel_grapheme("🇪🇺X") == std::pair{uct::grapheme{"🇪🇺", 2}, "X"sv});
static_assert(uct::peel_grapheme("äb") == std::pair{uct::grapheme{"ä", 1}, "b"sv});
static_assert(uct::peel_grapheme("X́ab") == std::pair{uct::grapheme{"X́", 1}, "ab"sv});
static_assert(uct::peel_grapheme("X́̉ab") == std::pair{uct::grapheme{"X́̉", 1}, "ab"sv});

static_assert(std::forward_iterator<uct::grapheme_iterator>,
              "grapheme_iterator is a forward-iterator");

static_assert(std::ranges::range<uct::grapheme_range>, "grapheme_range is range");
static_assert(std::ranges::forward_range<uct::grapheme_range>, "grapheme_range is forward_range");
static_assert(std::ranges::viewable_range<uct::grapheme_range>, "grapheme_range is a viewable_range");

TEST_CASE("grapheme range iteration", "[graphemes]") {
	const auto range = uct::grapheme_range{"a𝕓🇪🇺⚧"};
	const auto end = range.end();
	auto it = range.begin();
	CHECK(it != end);
	CHECK(*it == uct::grapheme{"a", 1});
	CHECK(*(++it) == uct::grapheme{"𝕓", 1});
	CHECK(*(it++) == uct::grapheme{"𝕓", 1});
	CHECK(*(it) == uct::grapheme{"🇪🇺", 2});
	CHECK(*(++it) == uct::grapheme{"⚧", 1});
	CHECK(it != end);
	CHECK(++it == end);
	CHECK(it != range.begin());
	CHECK(range.begin() == range.begin());
}
static_assert(std::ranges::range<uct::grapheme_view>, "grapheme_view is range");
static_assert(std::ranges::view<uct::grapheme_view>, "grapheme_view is view");
static_assert(std::ranges::forward_range<uct::grapheme_view>, "grapheme_view is forward_range");

TEST_CASE("grapheme view iteration", "[graphemes]") {
	const auto str = std::string{"a𝕓🇪🇺⚧"};
	const auto range = uct::grapheme_view{str};
	const auto end = range.end();
	auto it = range.begin();
	CHECK(it != end);
	CHECK(*it == uct::grapheme{"a", 1});
	CHECK(*(++it) == uct::grapheme{"𝕓", 1});
	CHECK(*(it++) == uct::grapheme{"𝕓", 1});
	CHECK(*(it) == uct::grapheme{"🇪🇺", 2});
	CHECK(*(++it) == uct::grapheme{"⚧", 1});
	CHECK(it != end);
	CHECK(++it == end);
	CHECK(it != range.begin());
	CHECK(range.begin() == range.cbegin());
}

