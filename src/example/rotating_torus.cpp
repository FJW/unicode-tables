#include "rotating_torus.hpp"

#include <algorithm>
#include <cmath>

#include <boxer/boxer.hpp>
#include <boxer/heatmap.hpp>
#include <cstddef>
#include <tuple>

namespace {
constexpr auto τ = std::numbers::pi_v<double> * 2;

inline constexpr double to_d(std::size_t n) { return static_cast<double>(n); }

inline constexpr auto dist(std::floating_point auto... x_i) {
	return std::sqrt(((x_i * x_i) + ...));
}

struct relative_coordinate {
	double x;
	double y;
	double z;
	double time;
};

constexpr auto sdf_rotate(std::invocable<relative_coordinate> auto f, double speed_factor = 1.0) {
	return [=](relative_coordinate coord) {
		const auto [x, y, z, time] = coord;
		const auto sin_time = std::sin(time * τ * speed_factor);
		const auto cos_time = std::cos(time * τ * speed_factor);
		const auto camera_x = x * cos_time - z * sin_time;
		const auto camera_z = x * sin_time + z * cos_time;
		return f({camera_x, y, camera_z, time});
	};
};

constexpr auto sdf_move(std::invocable<relative_coordinate> auto f, double speed_factor = 1.0) {
	return [=](relative_coordinate coord) {
		const auto [x, y, z, time] = coord;
		const auto camera_x = x + std::cos(time * τ * speed_factor);
		return f({camera_x, y, z, time});
	};
};

inline constexpr auto torus_sdf(unsigned index, double outer_radius = 0.4,
                                double thickness = 0.15) {
	return [=](relative_coordinate coord) {
		const auto [x, y, z, time] = coord;
		const double xy_d = dist(x, y) - outer_radius;
		const double d = dist(xy_d, z);
		return std::pair{d - thickness / 2, index};
	};
}

inline constexpr auto deathstar_sdf(unsigned index) {
	return [index](relative_coordinate coord) {
		const auto [x, y, z, time] = coord;
		auto big = dist(x, y, z) - 0.2;
		auto small = (dist(x - 0.15, y - 0.30, z + 0.30) - 0.3) * -1;

		return std::pair{std::max(big, small), index};
	};
}

constexpr std::invocable<relative_coordinate> auto
sdf_union(std::invocable<relative_coordinate> auto... f) {
	return [=](relative_coordinate coord) { return std::min({f(coord)...}); };
};

inline constexpr relative_coordinate center_coordinates(boxer::coordinate c) {
	const auto minmax = to_d(std::min(c.x_max, c.y_max));
	const auto dx = to_d(c.x_max) / (2 * minmax);
	const auto dy = to_d(c.y_max) / (2 * minmax);
	const auto x = (to_d(c.x) / minmax) - dx;
	const auto y = (to_d(c.y) / minmax) - dy;
	const auto time = to_d(c.z) / to_d(c.z_max);
	return {x, y, -2.0, time};
};

inline constexpr boxer::rgb_color color_shader(std::pair<double, unsigned> arg) {
	const auto [height, index] = arg;
	const auto intensity = std::clamp(0.0, height, 1.0);
	const auto τ = 2 * std::numbers::pi;
	const auto to_byte = [](double d) {
		return static_cast<std::uint8_t>(std::round(d * 255.0));
	};
	return {to_byte(intensity * 0.5 * (1 + std::sin(index))),
	        to_byte(intensity * 0.5 * (1 + std::sin(index + τ / 3))),
	        to_byte(intensity * 0.5 * (1 + std::sin(index + 2 * τ / 3)))};
}

inline constexpr auto make_shape_shader(std::invocable<relative_coordinate> auto distance_function,
                                        unsigned max_steps = 30u) {
	return [=](relative_coordinate mapped_coordinates) -> std::pair<double, unsigned> {
		constexpr double ε = 0.001;
		const auto [x, y, z, time_rel] = mapped_coordinates;
		double ray_depth = z;
		for (uint32_t i = 0; i < max_steps; ++i) {
			const auto [d, object] = distance_function({x, y, ray_depth, time_rel});

			if (d <= 0.01) {
				double normal_x =
				        distance_function({x + ε, y, ray_depth, time_rel}).first -
				        distance_function({x - ε, y, ray_depth, time_rel}).first;
				double normal_y =
				        distance_function({x, y + ε, ray_depth, time_rel}).first -
				        distance_function({x, y - ε, ray_depth, time_rel}).first;
				double normal_z =
				        distance_function({x, y, ray_depth + ε, time_rel}).first -
				        distance_function({x, y, ray_depth - ε, time_rel}).first;

				double inverse_norm = 1.0 / dist(normal_x, normal_y, normal_z);
				normal_x *= inverse_norm;
				normal_y *= inverse_norm;
				normal_z *= inverse_norm;

				return std::pair{(normal_y + normal_x) / 4 + .5, object};
			} else {
				ray_depth += d;
			}
		}
		return std::pair{0.0, 0u};
	};
}

} // anonymous namespace

void play_rotating_torus() {
	auto box = boxer::box{"Rotating Donut"};
	boxer::print_pixmaps(
	        box, 0, 0, 2000, std::chrono::milliseconds{0}, center_coordinates,
	        make_shape_shader(sdf_union(sdf_rotate(torus_sdf(1), 2.0),
	                                    sdf_move(sdf_rotate(deathstar_sdf(2), -6.0), 2.0))),
	        color_shader);
}
